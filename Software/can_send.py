#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import serial
import csv

'''
Created on Sat Sep 7 09:58:23 2019

@author: Nuttawat Punpigul
'''

# ser = serial.Serial(
#         port='COM12', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
#         baudrate = 115200,
#         timeout=1
# )

humid = 5000
temp = 2500

counter = 0
# can_id = hex(101)

# bytearray for CAN bus
# data = [id1,id2,id3,id4,ext,rtr,data1,data2,data3,data4,data5,data6,data7,data8]
data = [0, 0, 0, 255, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0]
# command = bytearray(data)

# while True:
#         ser.write(data)
#         print(data)
#         print("Write counter: %d \n"%(counter))
#         time.sleep(1)
#         counter += 1
#         print("Listening")
#        while True:
#                read = ser.readline()
#                if read != b'':
#                        # read = b'\x00\x00\x00e\x01\x00\x00\x00\x00\x00\x00\x00'
#                        print(read)
#                        break
        
def read_sensor(select_row):
    with open('//192.168.137.22/share/data_handling/payload.csv', 'r+') as readfile:
        reader = csv.DictReader(readfile)
        for row in reader:
            if int(row['No.']) == select_row:
                device = "{:.2f}".format(int(row['Device'])/100)
                temp = row['Temp']
                humid = row['Humid']
                return temp, humid, device

print("request sensor data")
time.sleep(1)
print("processing")
time.sleep(1)
# msg = str_can.encode2byte('rpcreqn#')           # send respond msg
# res_msg = [0, 0, 0x01, 0x00, 0, 0,
#                 msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
# ser.write(res_msg)
print("sending")
row = 1
while True:
        try:
                t, h, d = read_sensor(row)
                row += 1
        except Exception as e:
                # print(e)
                print(row-1)
                print("Done")
                break