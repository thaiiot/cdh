#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import os
import random
from smbus2 import SMBus, i2c_msg

'''
Created on Sat Sep 13 14:25 2020

@author: Nuttawat Punpigul

Command fuction of the S-band communication subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1
sup_addr = 0x46 # supervisor address
mss_addr = 0x45 # mission address

def request_data(i2c_addr, register, size = 0, data = [], read = True):
    time.sleep(0.1)
    buf = [register] + data
    write = i2c_msg.write(i2c_addr, buf)
    bus.i2c_rdwr(write)
    time.sleep(0.1)
    if read:
        read = i2c_msg.read(i2c_addr, size)
        bus.i2c_rdwr(read)
        time.sleep(0.1)
        return list(read)

### SUP unit ###
def Supervisor_Software_Reset():
    register = 0xAA
    request_data(sup_addr, register, read = False)
    time.sleep(0.05)

def Set_Supervisor_Mode(mode):
    register = 0xA0
    data = [int(mode)]
    request_data(sup_addr, register, data = data, read = False)
    time.sleep(0.05)

def Clear_I2C_Watchdog_Timer():
    register = 0xC0
    print('Reset S-band watchdog')
    request_data(sup_addr, register, read = False)

#FIXME: raw value conversion
def Get_Supervisor_Housekeeping_Telemetry(show = False):
    register = 0xB0
    read = request_data(sup_addr, register, size = 40)
    print (read)
    status = list(map(int, f'{read[38]:08b}'))
    if show:
        print('Reflected Power:', "{:.2f}".format(((int.from_bytes(read[0:2], byteorder='little', signed=False))/4095)*2500), 'V')
        # print('Reflected Power:', (int.from_bytes(read[0:2], byteorder='little', signed=False)))
        print('Forward Power:', "{:.2f}".format(((int.from_bytes(read[2:4], byteorder='little', signed=False))/4095)*2500), 'V')
        # print('Forward Power:', (int.from_bytes(read[2:4], byteorder='little', signed=False)))
        print('3V3 Line Voltage Monitor 1:', "{:.2f}".format((((read[5]<<8 | read[4])/4095)*2500*8)/1000), 'V')
        print('3V3 SW Line Voltage Monitor 1:', "{:.2f}".format((((read[7]<<8 | read[6])/4095)*2500*8)/1000), 'V')
        print('5V SW Line Voltage Monitor 1:', "{:.2f}".format((((read[9]<<8 | read[8])/4095)*2500*8)/1000), 'V')
        print('Battery Line Voltage Monitor 1:', "{:.2f}".format((((read[11]<<8 | read[10])/4095)*2500*8)/1000), 'V')
        print('3V3 Line Current Monitor 2:', (((read[13]<<8 | read[12]))/4095)*2500*8, 'mA')
        print('3V3 SW Line Current Monitor 2:', ((read[15]<<8 | read[14])/(1.5*4095))*2500, 'mA')
        print('5V SW Line Current Monitor 2:', (((read[17]<<8 | read[16]))/4095)*2500, 'mA')
        print('Battery Line Current Monitor 2:', (((read[19]<<8 | read[18]))/4095)*2500, 'mA')
        print('Voltage Control Line Monitor:', (((read[21]<<8 | read[20]))/4095)*2500, 'V')
        print('Temperature Driver:', ((read[23]<<8 | read[22])*-0.07669)+195.6037, '*C')
        print('Temperature Power Detector:', ((((((read[25]<<8 | read[24])/4095)*2500)-1.4)/0.005)+25), '*C')
        print('Temperature Power Amplifier:', "{:.2f}".format(((read[27]<<8 | read[26])*-0.07669)+195.6037), '*C')
        print('Temperature TCXO:', "{:.2f}".format(((read[29]<<8 | read[38])*-0.07669)+195.6037), '*C')
        print('Current state:', end = '')
        if status[7] == 0:
            print('\tTXS is on supervisor only mode')
        else:
            print('\tTXS is on stand-by mode')
        if status[6] == 0:
            print('\t\t3V3 SW good line low')
        else:
            print('\t\t3V3 SW good line high')
        if status[5] == 0:
            print('\t\t5V SW good line low')
        else:
            print('\t\t5V SW good line high')
        if status[4] == 1:
            print('\t\tPA over temperature')
        if status[3] == 1:
            print('\t\tTCXO over temperature')
        if status[2] == 1:
            print('\t\t5V line over current ')
        print('Watchdog Timer Reset Counter', (read[39]))

def Get_Uptime():
    register = 0xB1
    read = request_data(sup_addr, register, size = 2)
    sup_time = (read[1]<<8 | read[0])
    print('Supervisor uptime:', sup_time, 's')

    register = 0xB2
    read = request_data(sup_addr, register, size = 2)
    mss_time = (read[1]<<8 | read[0])
    print('MSS uptime:', mss_time, 's')

def Get_Supervisor_Status():
    register = 0xB4
    response = request_data(sup_addr, register, size = 1)
    status = list(map(int, f'{response[0]:08b}'))
    print(status)
    print('Supervisor status:', end = '')
    if status[7] == 0:
        print('\tTXS is on supervisor only mode')
    else:
        print('\tTXS is on stand-by mode')
    if status[6] == 0:
        print('\t\t\t3V3 SW good line low')
    else:
        print('\t\t\t3V3 SW good line high')
    if status[5] == 0:
        print('\t\t\t5V SW good line low')
    else:
        print('\t\t\t5V SW good line high')
    if status[4] == 1:
        print('\t\t\tPA over temperature')
    if status[3] == 1:
        print('\t\t\tTCXO over temperature')
    if status[2] == 1:
        print('\t\t\t5V line over current ')

### MSS unit ###
def Set_Tx_Frequency(freq = 2250000):
    register = 0xA0
    data = list(int.to_bytes(freq,length=4,byteorder='little',signed=False))
    request_data(mss_addr, register, data = data, read = False)
    time.sleep(0.05)

def Set_Tx_Mode(mode):
    register = 0xB0
    data = [int(mode)]
    request_data(mss_addr, register, data = data, read = False)
    time.sleep(0.05)

def Set_Bitrate():
    register = 0xA4
    data = [0]
    request_data(mss_addr, register, data = data, read = False)
    time.sleep(0.05)
    print('Set bitrate to lowest')

def Set_Modulation_Scheme(mode = 0):
    register = 0xA5
    data = [mode]
    request_data(mss_addr, register, data = data, read = False)
    time.sleep(0.05)

def Write_and_Send_Frame():
    register = 0xD0
    data = [0xFF, 0xFE] + random.sample(range(1,254), 219) + [0xFE, 0xFF]
    request_data(mss_addr, register, data = data, read = False)
    print('Send dummy data:')
    print(data)
    time.sleep(0.05)

def Send_Buffer_Frame():
    register = 0xD1
    data = []
    request_data(mss_addr, register, data = data, read = False)
    time.sleep(0.05)

def Get_Tx_Frequency():
    register = 0xE0
    read = request_data(mss_addr, register, size = 4)
    freq = int.from_bytes(read[0:4], byteorder='little', signed=False)
    print('Tx Frequency:', freq)
    time.sleep(0.05)

def Get_General_Status():
    register = 0xE1
    read = request_data(mss_addr, register, size = 26)
    time.sleep(0.05)
    print('Current Modulation', read[0])
    print('Tx On Current Status', read[1])
    print('Current Bitrate', read[2])
    print('Attenuator Value', read[3])
    print('Modulation output power', read[4])
    print('LUT choice', read[5])
    print('SCID', int.from_bytes(read[6:8], byteorder='little', signed=False))
    print('Voltage Serial DAC', int.from_bytes(read[8:10], byteorder='little', signed=False))
    print('Engine Status', int.from_bytes(read[10:14], byteorder='little', signed=False))
    print('Uptime (s)', int.from_bytes(read[14:18], byteorder='little', signed=False))
    print('Modulator PLL Lock error counter', int.from_bytes(read[18:20], byteorder='little', signed=False))
    print('Modulator frequency error counter', int.from_bytes(read[20:22], byteorder='little', signed=False))
    print('SPI error counter', int.from_bytes(read[22:26], byteorder='little', signed=False))


def Get_Engine_Status():
    register = 0xC8
    read = request_data(mss_addr, register, size = 2)
    print('Raw engine status value:', int.from_bytes(read[0:4], byteorder='little', signed=False))
    time.sleep(0.05)

def Set_Lock_Time():
    register = 0xB1
    data = list((int(input('Set lock time(s):'))).to_bytes(length=4,byteorder='little', signed=False))
    request_data(mss_addr, register, data = data, read = False)
    time.sleep(0.05)

def main():
    print('\n')
    print('Supervisor unit')
    print('1: Supervisor Software Reset')
    print('2: Set Supervisor Mode')
    print('3: Clear I2C Watchdog Timer')
    print('4: Get Supervisor Housekeeping Telemetry')
    print('5: Get Uptime')
    print('6: Get Supervisor Status')
    print('\nMSS unit')
    print('7: Set Tx Frequency')
    print('8: Set Tx Mode')
    print('9: Set Modulation Scheme')
    print('10: Write and Send Frame')
    print('11: Send Buffer Frame')
    print('12: Get Tx Frequency')
    print('13: Get General Status')
    print('14: Get Engine Status')
    print('15: Set Lock Time')
    print('00: ')
    print('\n')

    cmd = input("Select: ")
    if cmd == '1':
        Supervisor_Software_Reset()
        time.sleep(1)
        Get_Supervisor_Status()
    if cmd == '2':
        print('\n0: TXS on Supervisor Only mode')
        print('\n1: TXS on Stand-by mode')
        mode = int(input("Mode:"))
        Set_Supervisor_Mode(mode)
        time.sleep(1)
        Get_Supervisor_Status()
    if cmd == '3':
        Clear_I2C_Watchdog_Timer()
    if cmd == '4':
        Get_Supervisor_Housekeeping_Telemetry(True)
    if cmd == '5':
        Get_Uptime()
    if cmd == '6':
        Get_Supervisor_Status()
    if cmd == '7':
        freq = int(input("Frequency in kHz: "))
        Set_Tx_Frequency(freq)
    if cmd == '8':
        print('\n0: TXS on Stand-by mode')
        print('\n1: TXS on Tx on mode')
        mode = int(input("Mode:"))
        Set_Tx_Mode(mode)
    if cmd == '9':
        print('\n0: BPSK')
        print('\n1: OQPSK')
        mode = int(input("Mode:"))
        Set_Modulation_Scheme(mode)
    if cmd == '10':
        print('Send the dummy of 223 bytes')
        Write_and_Send_Frame()
    if cmd == '11':
        print('Trigger send buffer frame')
        Send_Buffer_Frame()
    if cmd == '12':
        Get_Tx_Frequency()
    if cmd == '13':
        Get_General_Status()
    if cmd == '14':
        Get_Engine_Status()
    if cmd == '15':
        Set_Lock_Time()


if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print('\nStop config S-TX')
            break