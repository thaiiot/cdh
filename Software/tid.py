#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import serial
import os
import str_can
import sys
from datetime import datetime

'''
Created on Thu Sep 26 2019 15:57

@author: Nuttawat Punpigul

Description: System health checker. If there are error on some service, record in log file.
'''

# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    ms = int(now.strftime("%M"))
    return d,m,y,hr,ms

# connect to Serial-CAN module
ser = serial.Serial(
        port ='/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 115200,
        timeout=1
)


# send the health status into the CAN bus
# will return 0 for active else inactive.
# TODO: create new id for error warning
while True:
    # using systemd command to check the status of software components
    pkg_fwd_stat = os.system('systemctl is-active --quiet single_chan_pkt_fwd')
    gateway_bridge_stat = os.system('systemctl is-active --quiet lora-gateway-bridge')
    server_stat = os.system('systemctl is-active --quiet loraserver')
    app_server_stat = os.system('systemctl is-active --quiet lora-app-server')
    mqtt_stat = os.system('systemctl is-active --quiet mosquitto')
    print("Package forwarder: ", pkg_fwd_stat)
    print("Gateway bridge: ", gateway_bridge_stat)
    print("Server: ", server_stat)
    print("AppServer: ", app_server_stat)
    print("MQTT: ", mqtt_stat)
    sys.stdout.flush()
    if pkg_fwd_stat or gateway_bridge_stat or server_stat or app_server_stat or mqtt_stat != 0:
        # send respond msg

        # get the error time
        day,month,year,hours,mins = get_datetime()
        error_log = str(day) + '/' + str(month) + '/' + str(year) + ' ' + str(hours) + ':' + str(mins) + ' ' + str(pkg_fwd_stat) + str(gateway_bridge_stat) + str(server_stat) + str(app_server_stat) + str(mqtt_stat)
        # save error log
        with open('\home\pi\health.txt', 'a') as outfile:
            outfile.write(error_log)
    else:
        msg = str_can.encode2byte('rpcreq1#')           # send respond msg
        res_msg = [0, 0, 0x01, 0x00, 0, 0,
                    msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
        ser.write(res_msg)
    time.sleep(5)
