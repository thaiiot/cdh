import time, socket, random, serial, os,sys
from datetime import datetime

# Serial connection setup
ser = serial.Serial('/dev/ttyS0', 115200, timeout=0.1)  # open serial connection
# print(ser.isOpen)     #  check the serial connection
# ser.open()
print('Open serial on', ser.name)    #  display port name

STID = 0x1A  # System Type Identifier (ICEPS)
IVID = 0x05  # Interface Version Identifier
BID = 0x01	 # Board Identifier

param = []
CMD_ID = 0x06

data = [0x3C, 0x63, 0x6D, 0x64, 0x3E] + [STID] + [IVID] + [CMD_ID] + [BID] + param + [0x3C, 0x2F, 0x63, 0x6D, 0x64, 0x3E]
ser.flushInput() #flush input buffer, discarding all its contents
ser.flushOutput() #flush output buffer, aborting current output and discard all that is in buffer
time.sleep(0.1)
ser.write(bytes(data))
time.sleep(0.1)
read = ser.readall()
data_buf = list(read)
# print("Reponse data:")
# print(data_buf)

if CMD_ID == 0x06:
    while True:
        ser.write(bytes(data))
        print('\n(0x06)Reset the watchdog timer')
        print('Write data:')
        # print(data)
        time.sleep(1)
        print("Reponse data:")
        # print(data_buf)
        ser.flushInput()
        ser.flushOutput()
        time.sleep(30)
        data = [0x3C, 0x63, 0x6D, 0x64, 0x3E] + [STID] + [IVID] + [0xA2] + [BID] + param + [0x3C, 0x2F, 0x63, 0x6D, 0x64, 0x3E]
        time.sleep(0.1)
        ser.write(bytes(data))
        time.sleep(0.1)
        read = ser.readall()
        data_buf = list(read)
        TEMP = (data_buf[14]<<8 | data_buf[13])/100
        VIP_OBC05 = data_buf[73:79]
        curr = (VIP_OBC05[3]<<8 | VIP_OBC05[2])/1000
        print('RPI current consuming:', curr)
        sys.stdout.flush()
        if curr < 0.3:
            os.system('sudo systemctl restart start_forwarder.service')
            print('Start LoRa Gateway')
            sys.stdout.flush()
            now = datetime.now().ctime()
            # save error log
            with open('/home/pi/lora_restart_log.txt', 'a') as outfile:
                outfile.write(now + '  ' + str(TEMP) + '\n')
            print(now + '  ' + str(TEMP))


time.sleep(0.1)
print('Write data:')
print(data)
time.sleep(1)
print("Reponse data:")
print(data_buf)
time.sleep(2)
