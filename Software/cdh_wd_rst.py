#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sys
import RPi.GPIO as GPIO

'''
Created on Sun Sep 26 14:55 2020

@author: Nuttawat Punpigul

Looping the Reset watchdog fuction of the command and data handling subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

# Reset pin on GPIO6
GPIO.setmode(GPIO.BCM)      # set up BCM GPIO numbering
GPIO.setup(6, GPIO.OUT)     # set GPIO6 as an output(WDI)
while True:
    sys.stdout.flush()
    print('Trigger the CDH watchdog timer reset signal')
    time.sleep(1)
    GPIO.output(6, 1)           # set port/pin value to 1/HIGH/True
    print('Trigger HIGH')
    time.sleep(2)
    GPIO.output(6, 0)           # set port/pin value to 0/LOW/False
    print('Trigger LOW')
    time.sleep(0.1)                  # wait 0.1 seconds
    # GPIO.cleanup()              # clean up after yourself

    time.sleep(30)                   # wait 30 seconds
