#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
from smbus2 import SMBus
# from smbus import SMBus

'''
Created on Sat Sep 11 14:25:20 2020

@author: Nuttawat Punpigul

Command and Data handling for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
		- add all function of each subsystem
'''

bus = SMBus(1) # indicates /dev/ic2-1

def Report_uptime():
	i2c_rxaddr = 0x60
	register = 0x40
	bus.read_i2c_block_data(i2c_rxaddr, register,length = 4)
	read_rx = bus.read_i2c_block_data(i2c_rxaddr, register,length = 4)
	# print(read_rx)
	print("")
	buf = (read_rx[3]<<8 | read_rx[2]<<8 | read_rx[1]<<8 | read_rx[0])
	print('Receiver uptime:', buf, 'second')

	time.sleep(0.05)

	i2c_txaddr = 0x61
	register = 0x40
	bus.read_i2c_block_data(i2c_txaddr, register,length = 4)
	read_tx = bus.read_i2c_block_data(i2c_txaddr, register,length = 4)
	# print(read_tx)
	print("")
	buf = (read_tx[3]<<8 | read_tx[2]<<8 | read_tx[1]<<8 | read_tx[0])
	print('Transmitter uptime:', buf, 'second')

TODO: """Merge this two function and use the parameter to select between TX or RX module"""
def Measure_all_the_telemetry_channels():
	i2c_addr = 0x61
	register = 0x25
	bus.read_i2c_block_data(i2c_addr, register,length = 18)
	read = bus.read_i2c_block_data(i2c_addr, register,length = 18)
	# print(read)
	print("")
	bus_volt = (read[5]<<8 | read[4]) * 0.00488
	bus_curr = (read[7]<<8 | read[6]) * 0.16643964
	tx_curr = (read[9]<<8 | read[8]) * 0.16643964
	rx_curr = (read[11]<<8 | read[10]) * 0.16643964
	pa_curr = (read[13]<<8 | read[12]) * 0.16643964
	pa_temp = (((read[15]<<8 | read[14])) * (-0.07669)) + 195.6037
	os_temp = (((read[17]<<8 | read[16])) * (-0.07669)) + 195.6037
	print('Power bus voltage :', round(bus_volt, 2), 'V')
	print('Total current consumption :', round(bus_curr	, 2), 'mA')
	print('Transmitter current consumption :', round(tx_curr, 2), 'mA')
	print('Receiver current consumption :', round(rx_curr, 2), 'mA')
	print('Power amplifier current consumption :', round(pa_curr, 2), 'mA')
	print('Power amplifier temperature :', round(pa_temp, 2), 'C')
	print('Local oscillator temp:', round(os_temp, 2), 'C')
	return bus_curr, bus_curr, tx_curr, rx_curr, pa_temp, pa_curr, os_temp

def Measure_all_the_telemetry_channels_last():
	i2c_addr = 0x61
	register = 0x26
	read = bus.read_i2c_block_data(i2c_addr, register,length = 18)
	# print(read)
	print("")
	bus_volt = (read[5]<<8 | read[4]) * 0.00488
	bus_curr = (read[7]<<8 | read[6]) * 0.16643964
	tx_curr = (read[9]<<8 | read[8]) * 0.16643964
	rx_curr = (read[11]<<8 | read[10]) * 0.16643964
	pa_curr = (read[13]<<8 | read[12]) * 0.16643964
	pa_temp = (((read[15]<<8 | read[14])) * (-0.07669)) + 195.6037
	os_temp = (((read[17]<<8 | read[16])) * (-0.07669)) + 195.6037
	print('Power bus voltage :', round(bus_volt, 2), 'V')
	print('Total current consumption :', round(bus_curr	, 2), 'mA')
	print('Transmitter current consumption :', round(tx_curr, 2), 'mA')
	print('Receiver current consumption :', round(rx_curr, 2), 'mA')
	print('Power amplifier current consumption :', round(pa_curr, 2), 'mA')
	print('Power amplifier temperature :', round(pa_temp, 2), 'C')
	print('Local oscillator temp:', round(os_temp, 2), 'C')
	return bus_curr, bus_curr, tx_curr, rx_curr, pa_temp, pa_curr, os_temp

def Send_frame(data):
	i2c_addr = 0x61
	register = 0x10
	print('Send msg to tx buffer')
	bus.write_i2c_block_data(i2c_addr,register,data)
	time.sleep(0.05)
	read = bus.read_i2c_block_data(i2c_addr, register,length = 1)
	print('TX buffer slot ramain:', int("".join(map(str, read))))

def Clear_beacon():
	print('Clear beacon')
	i2c_addr = 0x61
	register = 0x1F
	data = [0x00]
	bus.write_i2c_block_data(i2c_addr,register,data)

def Set_beacon(interval=10):
	Clear_beacon()
	time.sleep(0.05)
	i2c_addr = 0x61
	register = 0x14
	byte_interval = list(interval.to_bytes(2, byteorder='little'))
	bus.read_i2c_block_data(i2c_addr, 0x25,length = 18)
	tele_read = bus.read_i2c_block_data(i2c_addr, 0x25,length = 18)
	print(tele_read)
	data = byte_interval + tele_read
	print('Set beacon interval to', interval , 'second')
	print('Msg sent:', data)
	bus.write_i2c_block_data(i2c_addr,register,data)
	time.sleep(0.05)

def Report_transmitter_state():
	i2c_rxaddr = 0x61
	register = 0x41
	bus.read_i2c_block_data(i2c_rxaddr, register,length = 1)
	read = bus.read_i2c_block_data(i2c_rxaddr, register,length = 1)
	# print(read)
	print("")
	state = "{0:b}".format(int("".join(map(str, read))))
	if state == '1100':
		print('Transmitter_state:  9600 bps/ beacon off / transmitter is turned off when idle')
	if state == '1101':
		print('Transmitter_state:  9600 bps/ beacon off / transmitter is turned on when idle')
	if state == '1110':
		print('Transmitter_state:  9600 bps/ beacon on/ transmitter is turned off when idle')
	if state == '1111':
		print('Transmitter_state:  9600 bps/ beacon on / transmitter is turned on when idle')

def Watchdog_reset(select):
	if select == 'rx':
		i2c_addr = 0x60
		register = 0xCC
		data = [0x00]
		bus.write_i2c_block_data(i2c_addr,register,data)
		print("")
		print('RX watchdog reset')
	if select == 'tx':
		i2c_addr = 0x61
		register = 0xCC
		data = [0x00]
		bus.write_i2c_block_data(i2c_addr,register,data)
		print("")
		print('TX watchdog reset')

def Get_number_of_frames_in_receive_buffer():
	i2c_addr = 0x60
	register = 0x21
	bus.read_i2c_block_data(i2c_addr, register,length = 2)
	read = bus.read_i2c_block_data(i2c_addr, register,length = 2)
	# print(read)
	print("")
	buf = read[1]<<8 | read[0]
	print('Number of frames in receive buffer :', buf)


def Get_frame_from_receive_buffer():
	i2c_addr = 0x60
	register = 0x22
	bus.read_i2c_block_data(i2c_addr, register,length = 32)
	read = bus.read_i2c_block_data(i2c_addr, register,length = 32)
	# print(read)
	print("")
	data_size = read[1]<<8 | read[0]
	doppler_freq = ((read[3]<<8 | read[2]) * 13.352) - 22300
	rssi = ((read[5]<<8 | read[4]) * 0.03) - 152
	print('Received', data_size, 'B/', 'Doppler frequency:', round(doppler_freq, 2), 'Hz/', 'RSSI:', round(rssi, 2), 'dBm')
	print('MSG:', read[6:(6+data_size)])
	print('Decode in ASCII:',"".join(list(map(chr, read[6:(6+data_size)]))))
	time.sleep(0.05)
	Remove_frame_from_buffer()

def Remove_frame_from_buffer():
	i2c_addr = 0x60
	register = 0x24
	data = [0x00]
	bus.write_i2c_block_data(i2c_addr,register,data)
	print("")
	print('Removed frame form buffer')

######################################

def main():
	print()
	print('1: Measure COMMU power status')
	print('2: Report COMMU uptime')
	print('3: COMMU TX test')
	print('4: Set beacon')
	print('5: Clear beacon')
	print('6: Report transmitter state')
	print('7: Check RX frame buffer')
	print('8: Get frame from RX buffer')
	print('9: Reset watchdog')
	print()

	cmd = input("Select:")
	if cmd == '1':
		Measure_all_the_telemetry_channels()
	if cmd == '2':
		Report_uptime()
	if cmd == '3':
		data = bus.read_i2c_block_data(0x61, 0x25,length = 18)
		print("MSG size:", len(data), 'bytes')
		Send_frame(data)
		time.sleep(1)
		Measure_all_the_telemetry_channels_last()
	if cmd == '4':
		interval = int(input("Input beacon interval (default 10 s): "))
		Set_beacon(interval)
	if cmd == '5':
		Clear_beacon()
	if cmd == '6':
		Report_transmitter_state()
	if cmd == '7':
		Get_number_of_frames_in_receive_buffer()
	if cmd == '8':
		Get_frame_from_receive_buffer()
	if cmd == '9':
		while True:
			try:
				Watchdog_reset('tx')
				time.sleep(0.05)
				Watchdog_reset('rx')
				time.sleep(30)
			except KeyboardInterrupt:
				break

if __name__ == "__main__":
	while True:
		main()
