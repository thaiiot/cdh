import serial, time
ser = serial.Serial('COM4', 115200, timeout = 0.1)  # open serial connection
# print(ser.isOpen)     #  check the serial connection
# ser.open()
print('Open serial on', ser.name)    #  display port name
count = 1
while count<3:
    data = [0x1F, 0x7F, 0xC4, 0x1F, 0xFF]
    ser.flushInput() #flush input buffer, discarding all its contents
    ser.flushOutput() #flush output buffer, aborting current output and discard all that is in buffer
    time.sleep(0.1)
    write = ser.write(bytes(data))
    read = ser.readall()
    print('send', bytes(data))
    # print('write:', write)
    print('read:', read)
    data_buf = list(read)
    print("request data")
    print('Got =', data_buf)
    for i in range(3,len(data_buf)-2):
        print(list(map(int, f'{data_buf[i]:08b}')))
    time.sleep(0.1)
    count += 1