#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import json

'''
Created on Sat Sep 07 09:30:42 2019

@author: Nuttawat Punpigul

Description: Login to LoRa app server via API and generate the JWT token for using in API service access.
The script will create a json file to store the generated JWT token.

Convert from CURL code to python code
TODO: - make this script running on system bootup
      - JWT token has 48 hr lifetime, make this script generate new JWT token every 24 hr
'''
def get_jwt():
    # API url (change the ip address to the App server ip)
    url = 'http://127.0.0.1:8080/api/internal/login'

    headers = { 'Content-Type': 'application/json', 'Accept': 'application/json'}

    # username and password for the LoRa server authentication
    data = '{ "password": "admin", "email": "admin" }'

    # request new jwt token
    response = requests.post(url, headers=headers, data=data)

    # parse 'response'
    jwt = json.loads(response.text)
    print("New jwt token received")
    # print(jwt["jwt"])

    # store new jwt token in a json file
    with open('/home/pi/data_handling/jwt.json', 'w') as outfile:
        outfile.write(jwt["jwt"])

if __name__ == '__main__':
  get_jwt()