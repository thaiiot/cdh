#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import time
import json
import csv
import sys

'''
Created on Sat Sep 07 09:30:42 2019

@author: Nuttawat Punpigul

Subscribe to uplink MQTT topic to get end devices sensor data and store them in a file.
TODO: - add connection checking
      - save file name "(dev_number)-(date)"
      - **make this script running on system bootup**
'''

'''
LoRa server MQTT topic template

uplink_topic_template   = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/rx"
downlink_topic_template = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/tx"
join_topic_template     = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/join"
ack_topic_template      = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/ack"
error_topic_template    = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/error"
status_topic_template   = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/status"
location_topic_template = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/location"
'''
# global variable 
counter = 1

def on_message(client, userdata, message):
    global counter
    # print("message received " ,str(message.payload.decode("utf-8")))
    data = json.loads(message.payload.decode("utf-8"))         # convert byte array to json object
    # print("message topic=",message.topic)
    print('\n')
    print("message received @ Freq", data['txInfo']['frequency'], 'data:', data['data'])
    print('Device no.', data['deviceName'])
    print('Msg no.', counter, 'Temp:', data['object']['temperature'], 'Humid:', data['object']['humidity'])
    counter += 1
    # print(json.dumps(data, indent=4))

    # write_file(dev_num, temp, humid)


broker_address="127.0.0.1"        # MQTT broker ip address
topic = "application/+/device/+/rx"     # topic for subscribing
# print("creating new instance")
client = mqtt.Client("P1")              # create new instance
client.on_message = on_message        # attach function to callback
print("connecting to broker", broker_address)
client.connect(broker_address)          # connect to broker
print("Subscribing to topic", topic)

while True:
    client.loop_start()                 # start the loop
    client.subscribe(topic)             # subscribing
    # client.publish("topic","msg")
    time.sleep(1)                       # wait
    # client.loop_stop()                #stop the loop
