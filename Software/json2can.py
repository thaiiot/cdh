#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

'''
Created on Sat Sep 14 19:48:42 2019

@author: Nuttawat Punpigul
'''

# get sensors data by read the json file
with open('devices.json', 'r') as dev_file:
  dev_list = json.loads(dev_file.read())

print(dev_list)
