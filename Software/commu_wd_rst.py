#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sys
from smbus2 import SMBus, i2c_msg

'''
Created on Thu Sep 23 13:25 2020

@author: Nuttawat Punpigul

Looping the Reset watchdog fuction of the communication subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1

def Watchdog_reset(select):
	if select == 'rx':
		i2c_addr = 0x60
		register = 0xCC
		data = [0x00]
		bus.write_i2c_block_data(i2c_addr,register,data)
		print("")
		print('RX watchdog reset')
	if select == 'tx':
		i2c_addr = 0x61
		register = 0xCC
		data = [0x00]
		bus.write_i2c_block_data(i2c_addr,register,data)
		print("")
		print('TX watchdog reset')

while True:
	sys.stdout.flush()
	Watchdog_reset('tx')
	time.sleep(0.05)
	Watchdog_reset('rx')
	time.sleep(45)