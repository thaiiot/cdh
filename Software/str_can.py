#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Created on Sat Sep 7 10:48:55 2019

@author: Nuttawat Punpigul

Description: Encode and decode string into byte format

TODO: - 
'''

def encode2byte(text):
    byte_string = str.encode(text, "utf-8")
    # print(byte_string)
    enc_msg = [i for i in byte_string]
    # print(enc_msg)

    return enc_msg

def decode2str(byte_string):
    received_text = "".join(chr(i) for i in byte_string)
    # print(received_text)
    
    return received_text

command = encode2byte('c')
response = encode2byte('r')
cdh = encode2byte('c')
eps = encode2byte('e')
tx = encode2byte('t')
rx = encode2byte('r')
adcs = encode2byte('a')
payload = encode2byte('p')
reset = encode2byte('rst')
request_data = encode2byte('req')
elec_on = encode2byte('eon')
elec_off = encode2byte('eof')
tx_on = encode2byte('ton')
tx_off = encode2byte('tof')
rx_on = encode2byte('ron')
rx_off = encode2byte('rof')
adcs_on = encode2byte('aon')
adcs_off =  encode2byte('aof')

# print(reset)
# print(decode2str(command))
test_command = encode2byte('ccpreq1#')
# sender bytearray
data = [0, 0, 0, 0x100,
        test_command[0], test_command[1], test_command[2], test_command[3], test_command[4], test_command[5], test_command[6], test_command[7]]
# print(data)

# receiver bytearray
hex_data = [hex(x) for x in data]
# print(hex_data)

command = bytearray(data[4:12]).decode("utf-8")
# print(command)
# # rcv bytearray from CAN bus reading 
# a = list(b'\x00\x00\x00\x64\x63\x63\x70\x72\x65\x71\x33\x23')
# print(a)
