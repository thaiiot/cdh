#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time, socket, random, serial
from datetime import datetime

'''
Created on Sat Sep 14 8:48 2020

@author: Nuttawat Punpigul

Command fuction for doing Hardware In the Loop test of the Attitude Determination and Control subsystem for the THAIIOT 3U cubesat.
Mathlab ---- Generate sensor input ----> TCP/IP ----> Python(This script) on CDH ---- CDH send simulation trigger command to ADCS ----> 
CHD request actuator output value from ADCS ----> TCP/IP ----> Mathlab(visualize new attitude of the satellite)

TODO: 	- make this script running on system bootup
'''
# Serial connection setup
ser = serial.Serial('COM4', 115200, timeout=0.1)  # open serial connection
# print(ser.isOpen)     #  check the serial connection
# ser.open()
print('Open serial on', ser.name)    #  display port name

# TCP connection setup
TCP_IP = '127.0.0.1'
TCP_PORT = 30001
BUFFER_SIZE = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
print('Waiting for Simulink to start')
s.listen(1)
conn, addr = s.accept()
print('Connection address: ', addr)

# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    mn = int(now.strftime("%M"))
    return d,m,y,hr,mn

#TODO: NADIR status check >>> measuring power >>> TX

def request_data(reg_addr, size, show = False):
    data = [0x1F, 0x7F] + [reg_addr] + [0x1F, 0xFF]
    ser.flushInput() #flush input buffer, discarding all its contents
    ser.flushOutput() #flush output buffer, aborting current output and discard all that is in buffer
    time.sleep(0.1)
    ser.write(bytes(data))
    read = ser.readall()
    data_buf = list(read)
    if show:
        print("request data")
        print('Got =', data_buf)
        for i in range(3, len(data_buf)-2):
            print(list(map(int, f'{data_buf[i]:08b}'))) # loop for show all data received in 8 bit format
    return data_buf

def command(tc_id, param = [], show = False):
    data = [0x1F, 0x7F] + [tc_id] + param + [0x1F, 0xFF]
    time.sleep(0.1)
    ser.write(bytes(data))
    read = list(ser.readall())
    print(read)
    if show:
        print("Command:", hex(tc_id))
        if read[3] != 0:
            err_stat = ''
            if read[3] == 1:
                err_stat = 'Invalid TC'
            if read[3] == 2:
                err_stat = 'Incorrect Length '
            if read[3] == 3:
                err_stat = 'Incorrect Parameter'
            if read[3] == 4:
                err_stat = 'CRC check failed'
            print('\n!!!!! ERROR !!!!!', err_stat, '\n')
            return
    # print(list(map(int, f'{data[3]:08b}')))
    print('>>>>> Successed!! <<<<<')

def ADCS_Triggered_Mode():
    command(10,[2])
    time.sleep(0.5)
    command(19,[])

def tcp_send(mag_x_buf, mag_y_buf, mag_z_buf, wheel_buf):
    # send data to Simulink
    mag_x = int.to_bytes(mag_x_buf,length=4,byteorder='little',signed=True)
    mag_y = int.to_bytes(mag_y_buf,length=4,byteorder='little',signed=True)
    mag_z = int.to_bytes(mag_z_buf,length=4,byteorder='little',signed=True)
    wheel = int.to_bytes(wheel_buf,length=4,byteorder='little',signed=True)

    conn.send(mag_x)
    conn.send(mag_y)
    conn.send(mag_z)
    conn.send(wheel)

    print('\n\nsent data:', [mag_x_buf, mag_y_buf, mag_z_buf, wheel_buf])

def tcp_rcv():
    # receive data from Simulink
    data = list(conn.recv(BUFFER_SIZE))
    # print('\nreceived data:', data)
    print('\nreceived data:')
    v_ecef_x = int.from_bytes(data[0:4], byteorder='little', signed=True)
    v_ecef_y = int.from_bytes(data[4:8], byteorder='little', signed=True)
    v_ecef_z = int.from_bytes(data[8:12], byteorder='little', signed=True)

    pos_ecef_x = int.from_bytes(data[12:16], byteorder='little', signed=True)
    pos_ecef_y = int.from_bytes(data[16:20], byteorder='little', signed=True)
    pos_ecef_z = int.from_bytes(data[20:24], byteorder='little', signed=True)

    #int16
    print('v_ecef_x:', v_ecef_x)
    print('v_ecef_y:', v_ecef_y)
    print('v_ecef_z:', v_ecef_z)
    #int32
    print('pos_ecef_x:', pos_ecef_x)
    print('pos_ecef_y:', pos_ecef_y)
    print('pos_ecef_z:', pos_ecef_z)

    return [v_ecef_x, v_ecef_y, v_ecef_z], [pos_ecef_x, pos_ecef_y, pos_ecef_z]


mag_x_buf = 0
mag_y_buf = 0
mag_z_buf = 0
wheel_buf = 0
# Check the ADCS connection
print('ADCS status')
print('1', end='')
request_data(0x84, 6, True)
print('2', end='')
request_data(0xE0, 6, True)
# ADCS_Enabled
# before set to triggered mode, we have to setup all of the control power via Enabled mode
print('Set to Enable mode')
command(0x0A,[0x01],True)
time.sleep(1)
print('Set CubeControl Signal')
command(0x0B,[0x01,0,0],True)
time.sleep(1)
print('Set CubeControl Motor')
command(0x0B,[0x05,0,0],True)
time.sleep(1)
print('Set Motor Power')
command(0x0B,[0x05,0,0x01],True)
time.sleep(1)
input('Stop and check ADCS status in CubeSupport')
# ADCS_Triggered_Mode
print('Set to trigger mode')
command(0x0A,[0x02],True)
time.sleep(0.5)
# Set Attitude Control Mode to Detumbling control
print('Set Attitude Control Mode to Detumbling control')
command(13,[0x01,0xFF,0xFF],True)
# Last readiness check before start the TCP communication
print('>>>>> Last Check <<<<<')
print(request_data(0x84, 6, True))
print('[16, 50, 192, 0, 0, 32] <<<< Must match this!!!')
input('Match??')
print('Start TCP communication')
for i in range(50):
    try:
        # mag_x_buf = random.randint(-10000, 10000)
        # mag_y_buf = random.randint(-10000, 10000)
        # mag_z_buf = random.randint(-10000, 10000)
        # wheel_buf = random.randint(-10000, 10000)
        tcp_send(mag_x_buf, mag_y_buf, mag_z_buf, wheel_buf)
        time.sleep(0.1)
        x_ecef, v_ecef = tcp_rcv()
        unix_time = list(int.to_bytes(12345678,length=4,byteorder='little',signed=False))
        css_raw1 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw2 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw3 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw4 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw5 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw6 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw7 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw8 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw9 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        css_raw10 = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        cam1_x = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        cam1_y = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        cam1_busy = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        cam1_result = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        cam2_x = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        cam2_y = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        cam2_busy = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        cam2_result = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        mag_x = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        mag_y = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        mag_z = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        rate_x = list(int.to_bytes(0,length=4,byteorder='little',signed=True))
        rate_y = list(int.to_bytes(0,length=4,byteorder='little',signed=True))
        rate_z = list(int.to_bytes(0,length=4,byteorder='little',signed=True))
        wheel_x = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        wheel_y = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        wheel_z = list(int.to_bytes(0,length=2,byteorder='little',signed=True))
        star1 = [0,0,0,0,0,0]
        star2 = [0,0,0,0,0,0]
        star3 = [0,0,0,0,0,0]
        gps_stat = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        gps_ref = list(int.to_bytes(0,length=2,byteorder='little',signed=False))
        gps_mili = list(int.to_bytes(0,length=4,byteorder='little',signed=False))
        pos_ecef_x = list(int.to_bytes(x_ecef[1],length=4,byteorder='little',signed=True))
        pos_ecef_y = list(int.to_bytes(x_ecef[2],length=4,byteorder='little',signed=True))
        pos_ecef_z = list(int.to_bytes(x_ecef[3],length=4,byteorder='little',signed=True))
        v_ecef_x = list(int.to_bytes(v_ecef[1],length=2,byteorder='little',signed=True))
        v_ecef_y = list(int.to_bytes(v_ecef[2],length=2,byteorder='little',signed=True))
        v_ecef_z = list(int.to_bytes(v_ecef[3],length=2,byteorder='little',signed=True))
        pos_stddev_x = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        pos_stddev_y = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        pos_stddev_z = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        vel_stddev_x = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        vel_stddev_y = list(int.to_bytes(0,length=1,byteorder='little',signed=False))
        vel_stddev_z = list(int.to_bytes(0,length=1,byteorder='little',signed=False))

        data = [unix_time, css_raw1, css_raw2, css_raw3, css_raw4, css_raw5, css_raw6, css_raw7, css_raw8, css_raw9, css_raw10, cam1_x, cam1_y, cam1_busy, cam1_result, cam2_x, cam2_y, cam2_busy, cam2_result, mag_x , mag_y, mag_z, rate_x, rate_y, rate_z, wheel_x, wheel_y, wheel_z, star1, star2, star3, gps_stat, gps_ref, gps_mili, pos_ecef_x, v_ecef_x,pos_ecef_y, v_ecef_y,pos_ecef_z, v_ecef_z, pos_stddev_x, pos_stddev_y, pos_stddev_z, vel_stddev_x, vel_stddev_y, vel_stddev_z]
        print('size ', len(data))
        print(data, end='(Must be 127)')

        input('Press ENTER to trigger the loop')
        # Set trigger parameter
        print('Set trigger parameter')
        command(19, data, True)
        time.sleep(0.5)
        # Trigger the loop
        print('Trigger the loop')
        command(18, show=True)
        # Request actuator output
        ####### Magnetorquer Configuration  Set 21 Get 136
        ####### Wheel Configuration         Set 22 Get 137
        ####### Wheel Speed Command                Get 158
        ####### Wheel Speed                        Get 156
        act_out = request_data(132, 6, True)

    except Exception as e:
        print(e)

# Close all connection port.
ser.close()
conn.close()