#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time, os
from datetime import datetime
from smbus2 import SMBus, i2c_msg
from bitstring import BitArray

'''
Created on Thu Jun 03 14:24 2021

@author: Nuttawat Punpigul

Command fuction for doing Hardware funtional test of the Attitude Determination and Control subsystem for the THAIIOT 3U cubesat.

TODO: 	- make this script running on system bootup
'''
# Serial connection setup

bus = SMBus(1) # indicates /dev/ic2-1

i2c_addr = 0x57  # ADCS I2C address

clear = lambda: os.system("clear")
# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    mn = int(now.strftime("%M"))
    return d,m,y,hr,mn

def bintodec(binary):
    return sum(val*(2**idx) for idx, val in enumerate(reversed(binary)))

def request_data(reg_addr, size, show = False):
    data = [reg_addr]
    time.sleep(0.1)
    write = i2c_msg.write(i2c_addr, data)
    bus.i2c_rdwr(write)
    read = i2c_msg.read(i2c_addr, size)
    bus.i2c_rdwr(read)
    data_buf = list(read)
    if show:
        print("request id:", reg_addr)
        # print('Got =', data_buf)
        for i in range(size):
            print('Bytes', i, ' ', list(map(int, f'{data_buf[i]:08b}')))
    return data_buf

def command(tc_id, param = [], show = False):
    data = [tc_id] + param
    time.sleep(0.1)
    write = i2c_msg.write(i2c_addr, data)
    bus.i2c_rdwr(write)                         # send Telecommand
    if show:
        print("Command:", end='')
        print(list(write))
    tele_ack(show)                              # poll Temecommand Acknowledge

def tele_ack(show = False):
    data = request_data(0xF0, 4)
    if data[1] != 1:
        data = request_data(0xF0, 4)
    if data[2] != 0:
        if data[2] == 1:
            err_stat = 'Invalid TC'
        if data[2] == 2:
            err_stat = 'Incorrect Length '
        if data[2] == 3:
            err_stat = 'Incorrect Parameter'
        if data[2] == 4:
            err_stat = 'CRC check failed'
        print('\nERROR:', err_stat, '\n')
        return
    if show:
        print("\nACK")
        # print(data)
        print('Last TLC ID:', data[0])
    # print(list(map(int, f'{data[3]:08b}')))
    # print('\nSuccessed\n')

def state_check():
    raw = request_data(132, 6)
    b1 = list(map(int, f'{raw[0]:08b}'))
    b2 = list(map(int, f'{raw[1]:08b}'))
    b3 = list(map(int, f'{raw[2]:08b}'))
    b4 = list(map(int, f'{raw[3]:08b}'))
    b5 = list(map(int, f'{raw[4]:08b}'))
    b6 = list(map(int, f'{raw[5]:08b}'))
    att_est_mode = bintodec(b1[4:])
    con_mode = bintodec(b1[:4])
    run_mode = bintodec(b2[6:8])
    asgp4_mode = bintodec(b2[4:6])
    if att_est_mode == 0: att_est_mode = 'No attitude estimation'
    if att_est_mode == 1: att_est_mode = 'MEMS rate sensing'
    if att_est_mode == 2: att_est_mode = 'Magnetometer rate filter'
    if att_est_mode == 3: att_est_mode = 'Magnetometer rate filter with pitch estimation'
    if att_est_mode == 4: att_est_mode = 'Magnetometer and Fine-sun TRIAD algorithm'
    if att_est_mode == 5: att_est_mode = 'Full-state EKF'
    if att_est_mode == 6: att_est_mode = 'MEMS gyro EKF'
    if att_est_mode == 7: att_est_mode = 'User coded estimation mode'
    if con_mode == 0: con_mode = 'No control'
    if con_mode == 1: con_mode = 'Detumbling control'
    if con_mode == 2: con_mode = 'Y-Thomson spin'
    if con_mode == 3: con_mode = 'Y-Wheel momentum stabilized - Initial Pitch Acquisition'
    if con_mode == 4: con_mode = 'Y-Wheel momentum stabilized - Steady State'
    if con_mode == 5: con_mode = 'XYZ-Wheel control'
    if con_mode == 6: con_mode = 'Rwheel sun tracking control'
    if con_mode == 7: con_mode = 'Rwheel target tracking control'
    if con_mode == 8: con_mode = 'Very Fast-spin Detumbling control '
    if con_mode == 9: con_mode = 'Fast-spin Detumbling control '
    if con_mode == 10: con_mode = 'User Specific Control Mode 1 '
    if con_mode == 11: con_mode = 'User Specific Control Mode 2 '
    if con_mode == 12: con_mode = 'Stop R-wheels'
    if con_mode == 13: con_mode = 'User Coded Control Mode'
    if con_mode == 14: con_mode = 'Sun-tracking yaw- or roll-only wheel control mode'
    if con_mode == 15: con_mode = 'Target-tracking yaw-only wheel control mode'
    if run_mode == 0: run_mode = 'Off'
    if run_mode == 1: run_mode = 'Enabled '
    if run_mode == 2: run_mode = 'Triggered'
    if run_mode == 3: run_mode = 'Simulation'
    if asgp4_mode == 0: asgp4_mode = 'Off'
    if asgp4_mode == 1: asgp4_mode = 'Trigger'
    if asgp4_mode == 2: asgp4_mode = 'Background'
    if asgp4_mode == 3: asgp4_mode = 'Augment'
    print('\n------------ ADCS state --------------')
    print('ADCS Run Mode:\t\t\t', run_mode)
    print('Attitude Estimation Mode:\t', att_est_mode)
    print('Control Mode:\t\t\t', con_mode)
    print('ASGP4 Mode:\t\t\t', asgp4_mode)
    print('CubeControl Signal:\t', str(bool(b2[3])))
    print('CubeControl Motor:\t', str(bool(b2[2])))
    print('CubeSense1:\t\t', str(bool(b2[1])))
    print('CubeSense2:\t\t', str(bool(b2[0])))
    print('CubeWheel1:\t\t', str(bool(b3[7])))
    print('CubeWheel2:\t\t', str(bool(b3[6])))
    print('CubeWheel3:\t\t', str(bool(b3[5])))
    print('CubeStar:\t\t', str(bool(b3[4])))
    print('GPS Receiver:\t\t', str(bool(b3[3])))
    print('GPS LNA Power:\t\t', str(bool(b3[2])))
    print('Motor Driver:\t\t', str(bool(b3[1])))
    print('Sun is Above Local Horizon:\t', str(bool(b3[0])))
    print('\n------------ Error flag --------------')
    print('CubeSense1 Communications Error:\t\t', str(bool(b4[7])))
    print('CubeSense2 Communications Error:\t\t', str(bool(b4[6])))
    print('CubeControl Signal Communications Error:\t', str(bool(b4[5])))
    print('CubeControl Motor Communications Error:\t\t', str(bool(b4[4])))
    print('CubeWheel1 Communications Error:\t\t', str(bool(b4[3])))
    print('CubeWheel2 Communications Error:\t\t', str(bool(b4[2])))
    print('CubeWheel3 Communications Error:\t\t', str(bool(b4[1])))
    print('CubeStar Communications Error:\t\t\t', str(bool(b4[0])))
    print('Magnetometer Range Error:\t\t\t', str(bool(b5[7])))
    print('Cam1 SRAM Overcurrent Detected:\t\t\t', str(bool(b5[6])))
    print('Cam1 3V3 Overcurrent Detected:\t\t\t', str(bool(b5[5])))
    print('Cam1 Sensor Busy Error:\t\t\t\t', str(bool(b5[4])))
    print('Cam1 Sensor Detection Error:\t\t\t', str(bool(b5[3])))
    print('Sun Sensor Range Error:\t\t\t\t', str(bool(b5[2])))
    print('Cam2 SRAM Overcurrent Detected:\t\t\t', str(bool(b5[1])))
    print('Cam2 3V3 Overcurrent Detected:\t\t\t', str(bool(b5[0])))
    print('Cam2 Sensor Busy Error:\t\t\t\t', str(bool(b6[7])))
    print('Cam2 Sensor Detection Error:\t\t\t', str(bool(b6[6])))
    print('Nadir Sensor Range Error:\t\t\t', str(bool(b6[5])))
    print('Rate Sensor Range Error:\t\t\t', str(bool(b6[4])))
    print('Wheel Speed Range Error:\t\t\t', str(bool(b6[3])))
    print('Coarse Sun Sensor Error:\t\t\t', str(bool(b6[2])))
    print('StarTracker Match Error:\t\t\t', str(bool(b6[1])))
    print('Star Tracker Overcurrent Detected:\t\t', str(bool(b6[0])))
    return 

def rate_log(show=True):
    est_ang = request_data(147, 6)
    est_ang_x = round((int.from_bytes(est_ang[0:2], byteorder='little', signed=True)) * 0.01, 2)
    est_ang_y = round((int.from_bytes(est_ang[2:4], byteorder='little', signed=True)) * 0.01, 2)
    est_ang_z = round((int.from_bytes(est_ang[4:6], byteorder='little', signed=True)) * 0.01, 2)
    sen_rate = request_data(155, 6)
    sen_rate_x = round((int.from_bytes(sen_rate[0:2], byteorder='little', signed=True)) * 0.01, 2)
    sen_rate_y = round((int.from_bytes(sen_rate[2:4], byteorder='little', signed=True)) * 0.01, 2)
    sen_rate_z = round((int.from_bytes(sen_rate[4:6], byteorder='little', signed=True)) * 0.01, 2)
    mag_field = request_data(151, 6)
    mag_field_x = round((int.from_bytes(mag_field[0:2], byteorder='little', signed=True)) * 0.01, 2)
    mag_field_y = round((int.from_bytes(mag_field[2:4], byteorder='little', signed=True)) * 0.01, 2)
    mag_field_z = round((int.from_bytes(mag_field[4:6], byteorder='little', signed=True)) * 0.01, 2)
    mag_cmd = request_data(157, 6)
    mag_cmd_x = round(int.from_bytes(mag_cmd[0:2], byteorder='little', signed=True), 2)
    mag_cmd_y = round(int.from_bytes(mag_cmd[2:4], byteorder='little', signed=True), 2)
    mag_cmd_z = round(int.from_bytes(mag_cmd[4:6], byteorder='little', signed=True), 2)
    if show:
        print('\nEstimated Angular Rates (deg/s):\t', est_ang_x, est_ang_y, est_ang_z)
        print('Rate Sensor Rates (deg/s):\t\t', sen_rate_x, sen_rate_y, sen_rate_z)
        print('Magnetic field Measurement (uT):\t', mag_field_x, mag_field_y, mag_field_z)
        print('Magnetorquer Commands (10ms units):\t', mag_cmd_x, mag_cmd_y, mag_cmd_z)
    return abs(est_ang_x), abs(est_ang_z), (est_ang_y)


def main():
    print('Choose test mode number')
    print('1. Detumbling')
    print('2. Y-momentum')
    print('3. Momentum dumping')
    print('4. Reverse to default setting')

    select = int(input('\nNumber: '))

    # Detumbling test
    if select == 1:
        print('Detumbling test')
        time.sleep(1)
        print('ADCS run mode: enable')
        command(10,[1])             # ADCS run mode: enable
        time.sleep(0.2)
        print('Power Control CubeControl Signal and/or Motor Power: On ')
        command(11,[1, 0, 0])      # Power Control CubeControl Signal and/or Motor Power = On (1), All others = Off (0)
        time.sleep(0.2)
        print('Set Estimation Mode\n1. MEMS rate sensing\n2. Magnetometer rate filter')
        est_mode = int(input('Select: '))
        command(14,[est_mode])             # Set Estimation Mode = Magnetometer rate filter (2) or MEMS rate sensing (1)
        time.sleep(1)

        state_check()
        input('\nCheck the status then press any key to continue...')

        print('\nSet SD card logging')
        command(104,[80, 22, 00, 40, 00, 00, 00, 00, 00, 00, 10, 0, 0])    # Set SD card loggind [[10 bytes of intend TLM], log period(2 bytes), log destination]
        time.sleep(60) #TODO: adjust range delay to 60s
        rate_x, rate_z, rate_y = rate_log(False)
        # print(rate_x, rate_z)
        print('Check Rate Sensor Rates >>> ', end='')
        while rate_x > 0.5 or rate_z > 0.5:
            print('Adjust X, Z rate')
            print('Set control mode: Detumbling (timeout 600 s)')
            command(13,[1] + list(int.to_bytes(600,length=2,byteorder='little',signed=False)))    # Set control mode: Detumbling timeout 600 s
            for i in range(600): #TODO: adjust range delay to 600s      # Show ADCS status every sec, for behavior observation
                time.sleep(1)
                rate_x_, rate_z_, rate_y_ = rate_log()
                if rate_x_ < rate_x:    # Check MTQ direction
                    rate_x = True
                if rate_z_ < rate_z:    # Check MTQ direction
                    rate_z = True
                if rate_x or rate_z:
                    print('>>>>> MTQ correct direction')
                else:
                    print('>>>>> MTQ wrong direction')
                    time.sleep(1)
                    input('Press any key to continue...')
                    break
            print('\nCheck Rate Sensor Rates >>> ', end='')
            rate_x, rate_z, rate_y = rate_log(False)
        print('Rate X, Z < 0.5\n')
        while rate_y > -1: #FIXME: check to rate y condition in real situation
            print('Adjust Y rate')
            print('Set control mode: Y-Thomson (timeout 600 s)')
            command(13,[2] + list(int.to_bytes(600,length=2,byteorder='little',signed=False)))    # Set control mode: Y-Thomson timeout 600 s
            for i in range(600): #TODO: adjust range delay to 600s       # Show ADCS status every sec, for behavior observation
                time.sleep(1)
                rate_x_, rate_z_, rate_y_ = rate_log()
            print('\nCheck Rate Sensor Rates >>> ', end='')
            rate_x, rate_z, rate_y = rate_log(False)
            # print(rate_y)
        print('Rate Y < -1\n')
        time.sleep(1)
        print('Set control mode: Y-Thomson Final')
        command(13,[2] + list(int.to_bytes(0,length=2,byteorder='little',signed=False)))    # Set control mode: Y-Thomson timeout 0 s
        print('Finished detumbling test\n')
        input('Press any key to continue...')

    # Y-momentum test
    if select == 2:
        print('Y-momentum test')
        time.sleep(1)
        command(10,[1])             # ADCS run mode: enable
        raw = request_data(132, 6)
        print('Check prerequisites...')     # Magnetometer rate filter with pitch estimation mode has been active for 1 min. Y-Thomson spin state held for at least 1 orbit prior to this.
        b1 = list(map(int, f'{raw[0]:08b}'))
        att_est_mode = bintodec(b1[4:])
        con_mode = bintodec(b1[:4])
        if att_est_mode == 3:
            print('\tY-Thomson spin state: OK')
            att_est_mode = True
        else:
            print('\tY-Thomson spin state: FAILED')
        if con_mode == 2:
            print('\tPitch estimation mode: OK')
            con_mode = True
        else:
            print('\tPitch estimation mode: FAILED')
        if not(att_est_mode) or not(con_mode):
            print('!!! Does not meet the prerequisites !!!')
            print('\nSet Control Mode: Y-Thomson Final')
            command(13,[2] + list(int.to_bytes(0,length=2,byteorder='little',signed=False)))    # Set control mode: Y-Thomson final
            time.sleep(1)
            print('Set Estimation Mode: Magnetometer rate filter with pitch estimation')
            command(14,[3])     # Set Estimation Mode = Magnetometer rate filter with pitch estimation
        time.sleep(1)
        print('\nSet SD card logging') #FIXME: duplicate data mask check with CubeSupport
        command(104,[00, 22, 00, 10, 2, 00, 00, 8, 00, 00, 10, 0, 0])    # Set SD card loggind [[10 bytes of intend TLM], log period(2 bytes), log destination]
        command(104,[00, 40, 00, 00, 00, 00, 00, 00, 00, 00, 10, 0, 0])    # Set SD card loggind [[10 bytes of intend TLM], log period(2 bytes), log destination]
        time.sleep(0.2)
        print('Estimation Parameters')
        print('\t- Copy Estimation Parameters')
        est_param = request_data(223, 31) #TODO: continue here
        print('\t- Set new Estimation Parameters')
        old_param = list(map(int, f'{est_param[28]:08b}'))
        new_param = [0, 0, 0, 0] + old_param[4:]
        est_param[28] = BitArray(new_param).uint
        command(104,est_param)
        time.sleep(1)
        print('\tDone..')
        print('\nSet Estimation Mode: Magnetometer rate filter with pitch estimation')
        command(14,[3])     # Set Estimation Mode = Magnetometer rate filter with pitch estimation
        time.sleep(1)
        print('Set Control Mode: Steady Y-Momentum')
        command(13,[2] + list(int.to_bytes(0,length=2,byteorder='little',signed=False)))    # Set control mode: Y-Thomson final



    # Momentum dumping test
    if select == 3:
        print('Momentum dumping test')
        time.sleep(1)
    
    # Reverse to default setting
    if select == 4:
        print('Reverse to default setting')
        time.sleep(5)
        print('Stop SD card logging')
        command(104,[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])    # Set SD card logging [[10 bytes of intend TLM], log period(2 bytes), log destination]
        print('Check SD card logging status')
        request_data(235, 13, True)
        time.sleep(0.2)
        print('Set Estimation Mode = Off')
        command(14,[0])             # Set Estimation Mode = Magnetometer rate filter (2) or MEMS rate sensing (1)
        time.sleep(0.2)
        print('Power Control CubeControl Signal and/or Motor Power: Off ')
        command(11,[0, 0, 0])      # Power Control CubeControl Signal and/or Motor Power = On (1), All others = Off (0)
        time.sleep(0.2)
        print('ADCS run mode: disable')
        command(10,[0])             # ADCS run mode: enable
        time.sleep(0.2)
        state_check()
        input('Press any key to continue...')


if __name__ == "__main__":
    while True:
        try:
            clear()
            main()
        except KeyboardInterrupt:
            print('\nStop software')
            break