#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import eps_i2c
import os
import ant_deploy
import random
from smbus2 import SMBus, i2c_msg

'''
Created on Sat Sep 13 14:25 2020

@author: Nuttawat Punpigul

Command fuction of the communication subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1

def Report_uptime():
    i2c_rxaddr = 0x60
    register = 0x40
    bus.read_i2c_block_data(i2c_rxaddr, register,length = 4)
    read_rx = bus.read_i2c_block_data(i2c_rxaddr, register,length = 4)
    # print(read_rx)
    print("")
    buf = (read_rx[3]<<8 | read_rx[2]<<8 | read_rx[1]<<8 | read_rx[0])
    print('Receiver uptime:', buf, 'second')

    time.sleep(0.05)

    i2c_txaddr = 0x61
    register = 0x40
    bus.read_i2c_block_data(i2c_txaddr, register,length = 4)
    read_tx = bus.read_i2c_block_data(i2c_txaddr, register,length = 4)
    # print(read_tx)
    print("")
    buf = (read_tx[3]<<8 | read_tx[2]<<8 | read_tx[1]<<8 | read_tx[0])
    print('Transmitter uptime:', buf, 'second')

TODO: """Merge this two function and use the parameter to select between TX or RX module"""
def Measure_all_the_telemetry_channels():
    i2c_addr = 0x61
    register = 0x25
    bus.read_i2c_block_data(i2c_addr, register,length = 18)
    read = bus.read_i2c_block_data(i2c_addr, register,length = 18)
    # print(read)
    print("")
    bus_volt = (read[5]<<8 | read[4]) * 0.00488
    bus_curr = (read[7]<<8 | read[6]) * 0.16643964
    tx_curr = (read[9]<<8 | read[8]) * 0.16643964
    rx_curr = (read[11]<<8 | read[10]) * 0.16643964
    pa_curr = (read[13]<<8 | read[12]) * 0.16643964
    pa_temp = (((read[15]<<8 | read[14])) * (-0.07669)) + 195.6037
    os_temp = (((read[17]<<8 | read[16])) * (-0.07669)) + 195.6037
    print('Power bus voltage :', round(bus_volt, 2), 'V')
    print('Total current consumption :', round(bus_curr	, 2), 'mA')
    print('Transmitter current consumption :', round(tx_curr, 2), 'mA')
    print('Receiver current consumption :', round(rx_curr, 2), 'mA')
    print('Power amplifier current consumption :', round(pa_curr, 2), 'mA')
    print('Power amplifier temperature :', round(pa_temp, 2), 'C')
    print('Local oscillator temp:', round(os_temp, 2), 'C')
    # return bus_curr, bus_curr, tx_curr, rx_curr, pa_temp, pa_curr, os_temp
    return read

def Measure_all_the_telemetry_channels_last():
    i2c_addr = 0x61
    register = 0x26
    read = bus.read_i2c_block_data(i2c_addr, register,length = 18)
    # print(read)
    print("")
    bus_volt = (read[5]<<8 | read[4]) * 0.00488
    bus_curr = (read[7]<<8 | read[6]) * 0.16643964
    tx_curr = (read[9]<<8 | read[8]) * 0.16643964
    rx_curr = (read[11]<<8 | read[10]) * 0.16643964
    pa_curr = (read[13]<<8 | read[12]) * 0.16643964
    pa_temp = (((read[15]<<8 | read[14])) * (-0.07669)) + 195.6037
    os_temp = (((read[17]<<8 | read[16])) * (-0.07669)) + 195.6037
    print('Power bus voltage :', round(bus_volt, 2), 'V')
    print('Total current consumption :', round(bus_curr	, 2), 'mA')
    print('Transmitter current consumption :', round(tx_curr, 2), 'mA')
    print('Receiver current consumption :', round(rx_curr, 2), 'mA')
    print('Power amplifier current consumption :', round(pa_curr, 2), 'mA')
    print('Power amplifier temperature :', round(pa_temp, 2), 'C')
    print('Local oscillator temp:', round(os_temp, 2), 'C')
    return bus_curr, bus_curr, tx_curr, rx_curr, pa_temp, pa_curr, os_temp

def Send_frame(data):
    Clear_beacon()
    i2c_addr = 0x61
    register = 0x10
    print('Send msg to tx buffer')
    bus.write_i2c_block_data(i2c_addr,register,data)
    time.sleep(0.05)
    read = bus.read_i2c_block_data(i2c_addr, register,length = 1)
    print('TX buffer slot ramain:', int("".join(map(str, read))))

def Clear_beacon():
    print('Clear beacon')
    i2c_addr = 0x61
    register = 0x1F
    data = [0x00]
    bus.write_i2c_block_data(i2c_addr,register,data)

def Set_beacon(interval=120):
    # Clear_beacon()
    time.sleep(0.05)
    i2c_addr = 0x61
    register = 0x14
    byte_interval = list(interval.to_bytes(2, byteorder='little'))
    # eps = [eps_i2c.Get_System_Status()]
    # comm = Report_transmitter_state()
    # adcs = [0x00, 0x00, 0x00, 0x04]
    # pkg_fwd_stat = 0x00
    # if os.system('systemctl is-active --quiet start_forwarder') != 0:
    #     pkg_fwd_stat = 0x01
    # pay = [pkg_fwd_stat] # LoRa stat
    # ant_dep1 = ant_deploy.request_data(0x31, 0xC3, 2) # UV
    # ant_dep2 = ant_deploy.request_data(0x33, 0xC3, 2) # LoRa
    # ant = ant_dep1 + ant_dep2
    # TODO: ^^^^^ get back to normal coding ^^^^^
    # FIXME: Finalize the msg frame with beacon
    # data = byte_interval + eps + comm + adcs + pay + ant
    # data = byte_interval + [0xFF] + byte_interval + eps + comm + adcs + pay + ant + [0xFF]
    data = byte_interval + [0xFF] + random.sample(range(1,254), 19) + [0xFF]
    print('Set beacon interval to', interval , 'second')
    # print('Msg sent:', byte_interval, eps, comm, adcs, pay)
    # print(data)
    bus.write_i2c_block_data(i2c_addr,register,data)
    time.sleep(0.05)
    Report_transmitter_state(True)

def Report_transmitter_state(show=False):
    i2c_addr = 0x61
    register = 0x41
    bus.read_i2c_block_data(i2c_addr, register,length = 1)
    read = bus.read_i2c_block_data(i2c_addr, register,length = 1)
    # print(read)
    if show == True:
        state = "{0:b}".format(int("".join(map(str, read))))
        print('Transmitter_state:', state)
    return read

def Watchdog_reset(select):
    if select == 'rx':
        i2c_addr = 0x60
        register = 0xCC
        data = [0x00]
        bus.write_i2c_block_data(i2c_addr,register,data)
        print("")
        print('RX watchdog reset')
    if select == 'tx':
        i2c_addr = 0x61
        register = 0xCC
        data = [0x00]
        bus.write_i2c_block_data(i2c_addr,register,data)
        print("")
        print('TX watchdog reset')

def Get_number_of_frames_in_receive_buffer():
    i2c_addr = 0x60
    register = 0x21
    bus.read_i2c_block_data(i2c_addr, register,length = 2)
    read = bus.read_i2c_block_data(i2c_addr, register,length = 2)
    buf = read[1]<<8 | read[0]
    print('Number of frames in receive buffer :', buf)
    return buf

def Get_frame_from_receive_buffer():
    i2c_addr = 0x60
    register = 0x22
    bus.read_i2c_block_data(i2c_addr, register,length = 32)
    read = bus.read_i2c_block_data(i2c_addr, register,length = 32)
    # print(read)
    print("")
    data_size = read[1]<<8 | read[0]
    doppler_freq = ((read[3]<<8 | read[2]) * 13.352) - 22300
    rssi = ((read[5]<<8 | read[4]) * 0.03) - 152
    print('Received', data_size, 'B/', 'Doppler frequency:', round(doppler_freq, 2), 'Hz/', 'RSSI:', round(rssi, 2), 'dBm')
    # print('Received', data_size, 'B/', 'Doppler frequency:', round(doppler_freq, 2), 'Hz/', 'RSSI: -18.5 dBm')
    print('MSG:', read[6:(6+data_size)])
    decoded = "".join(list(map(chr, read[6:(6+data_size)])))
    print('Decode in ASCII:',decoded)
    time.sleep(0.05)
    Remove_frame_from_buffer()
    return decoded

def Remove_frame_from_buffer():
    i2c_addr = 0x60
    register = 0x24
    data = [0x00]
    bus.write_i2c_block_data(i2c_addr,register,data)
    print("")
    print('Removed frame form buffer')

def TX_performance():
    # data = [send frame register] + [data...] upto 206 bytes
    # data = [0x10] + eps_i2c.request_data(0xA0, 116) + eps_i2c.request_data(0xA2, 90)
    data = [0x10] + random.sample(range(1,254), 200) # TODO: ^^^^^ get back to normal coding ^^^^^
    i2c_addr = 0x61
    time.sleep(1)
    print('Send msg to tx buffer')
    write = i2c_msg.write(i2c_addr, data)
    bus.i2c_rdwr(write)

def Set_transmission_bitrate(bitrate=9600):
    i2c_addr = 0x61
    register = 0x28
    if bitrate == 1200:
        data = [0x01]
    if bitrate == 2400:
        data = [0x02]
    if bitrate == 4800:
        data = [0x04]
    if bitrate == 9600:
        data = [0x08]
    print('Set TX bitrate to', bitrate)
    bus.write_i2c_block_data(i2c_addr,register,data)
    print("")
    time.sleep(0.5)
    Report_transmitter_state()

######################################

def main():
    print()
    print('1: Measure COMMU power status')
    print('2: Report COMMU uptime')
    print('3: COMMU TX test')
    print('4: Set beacon')
    print('5: Clear beacon')
    print('6: Report transmitter state')
    print('7: Check RX frame buffer')
    print('8: Get frame from RX buffer')
    print('9: Reset watchdog')
    print('10: COMMU TX test 206 bytes')
    print('11: Set TX bitrate')
    print()

    cmd = input("Select:")
    if cmd == '1':
        Measure_all_the_telemetry_channels()
    if cmd == '2':
        Report_uptime()
    if cmd == '3':
        data = [0xFF] + bus.read_i2c_block_data(0x61, 0x25,length = 18) + Report_transmitter_state(show=False) + [0xFF]
        print("MSG size:", len(data), 'bytes')
        Send_frame(data)
        time.sleep(1)
        Measure_all_the_telemetry_channels_last()
    if cmd == '4':
        interval = int(input("Input beacon interval (default 60 s): "))
        Set_beacon(interval)
    if cmd == '5':
        Clear_beacon()
    if cmd == '6':
        Report_transmitter_state(True)
    if cmd == '7':
        Get_number_of_frames_in_receive_buffer()
    if cmd == '8':
        Get_frame_from_receive_buffer()
    if cmd == '9':
        while True:
            try:
                Watchdog_reset('tx')
                time.sleep(0.05)
                Watchdog_reset('rx')
                time.sleep(30)
            except KeyboardInterrupt:
                break
    if cmd == '10':
        TX_performance()
    if cmd == '11':
        bitrate = int(input('Select bitrate (1200, 2400, 4800, 9600): '))
        Set_transmission_bitrate(bitrate)

if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print('\nStop config COMMU')
            break