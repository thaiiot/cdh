#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
from datetime import datetime
from smbus2 import SMBus, i2c_msg

'''
Created on Sat Sep 14 8:48 2020

@author: Nuttawat Punpigul

Command fuction of the communication subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1

i2c_addr = 0x20  # EPS I2C address
STID = 0x1A  # System Type Identifier (ICEPS)
IVID = 0x05  # Interface Version Identifier
BID = 0x01	 # Board Identifier

def hex_show(data):
    h = []
    for buf in data:
        h.append(hex(buf))
    print(h)
    # return h

def request_data(CC, size, param = [], show = False):
    data = [STID, IVID, CC, BID] + param
    time.sleep(0.1)
    write = i2c_msg.write(i2c_addr, data)
    read = i2c_msg.read(i2c_addr, size)
    bus.i2c_rdwr(write, read)
    data = list(read)
    if data[2] != (CC + 1):
        # print('Wrong Response, retrying...')
        time.sleep(0.1)
        bus.i2c_rdwr(write, read)
        data = list(read)
    # hex_show(data)
    if show == True:
        print('')
        if data[4] == 0x80:
            print("Response: Accepted")
        if data[4] == 0x81:
            print("Response: Rejected")
        if data[4] == 0x82:
            print("Response: Invalid command code")
        if data[4] == 0x83:
            print("Response: Parameter missing")
        if data[4] == 0x84:
            print("Response: Parameter invalid")
        if data[4] == 0x85:
            print("Response: Unavailable in current mode/configuration")
        if data[4] == 0x86:
            print("Response: invalid system type, interface version or bid")
        if data[4] == 0x87:
            print("Response: internal error occurred during processing")
    return data

def No_operation():
    print("\n(0x02) System handshake")
    request_data(0x02, 5)

def System_Reset():
    print("\n(0xAA) System Reset")
    param = [0xA6]
    request_data(0xAA, 5, param)

def Cancel_Operation():
    print("\n(0x04) Cancel Operation")
    request_data(0x04, 5)

def Watchdog_reset():
    print("\n(0x06) Resets the watchdog timer")
    request_data(0x06, 5)

def Output_Bus_Channel_On(channel, show = False):
    if show:
        print("\n(0x16) Turn-on output power channels No.", channel)
        request_data(0x16, 5, [channel], True)
    request_data(0x16, 5, [channel])

def Output_Bus_Channel_Off(channel, show = False):
    if show:
        print("\n(0x18) Turn-off output power channels No.", channel)
        request_data(0x18, 5, [channel], True)
    request_data(0x18, 5, [channel])

def Switch_to_Nominal_Mode():
    print("\n(0x30) Switch to Nominal Mode")
    request_data(0x30, 5)

def Switch_to_Safety_Mode():
    print("\n(0x32) Switch to Safety Mode")
    request_data(0x32, 5)

def Get_System_Status(show = False):
    response = request_data(0x40, 36, show=show)
    # print(list(response))
    mode = response[5]
    if show == True:
        print("\n(0x40) Get System Status")
        print('')
        if mode == 0:
            print('Mode: STARTUP')
        if mode == 1:
            print('Mode: NOMINAL')
        if mode == 2:
            print('Mode: SAFTY')
        if mode == 3:
            print('Mode: EMERGENCY LOW POWER')
    # conf = response[6]
    # if conf == 0:
    # 	print('Read/write configuration parameters have been changed: no')
    # if conf == 1:
    # 	print('Read/write configuration parameters have been changed: yes')
    # reset_cause = response[7]
    # if reset_cause == 0:
    # 	print('Cause of last reset: power-on')
    # if reset_cause == 1:
    # 	print('Cause of last reset: watchdog')
    # if reset_cause == 2:
    # 	print('Cause of last reset: commanded')
    # if reset_cause == 3:
    # 	print('Cause of last reset: control system reset')
    # if reset_cause == 4:
    # 	print('Cause of last reset: emlopo')
    # uptime = (response[11]<<8 | response[10]<<8 | response[9]<<8 | response[8])
    # print('System uptime:', uptime, 'second')
    # error = (response[13]<<8 | response[12])
    # print('First internal error encountered during the system control cycle:', error)
    # RC_CNT_PWRON = (response[15]<<8 | response[14])
    # print('Power-on reset counter:', RC_CNT_PWRON)
    # RC_CNT_WDG = (response[17]<<8 | response[16])
    # print('Watchdog reset counter:', RC_CNT_WDG)
    # RC_CNT_CMD = (response[19]<<8 | response[18])
    # print('Commanded reset counter:', RC_CNT_CMD)
    # RC_CNT_MCU = (response[21]<<8 | response[20])
    # print('EPS controller reset counter:', RC_CNT_MCU)
    # RC_CNT_EMLOPO = (response[23]<<8 | response[22])
    # print('Emergency low power reset counter:', RC_CNT_EMLOPO)
    # PREVCMD_ELAPSED = (response[25]<<8 | response[24])
    # print('Time elapsed between reception of the previous and this command:', PREVCMD_ELAPSED,'second')
    # UNIX_TIME = (response[29]<<8 | response[28]<<8 | response[27]<<8 | response[26])
    # print('UNIX time:', UNIX_TIME)
    return mode

def Get_Housekeeping_Data(show = False):
    response = request_data(0xA2, 116, show=show)
    # print(list(response))
    VOLT_BRDSUP = (response[7]<<8 | response[6])
    TEMP = (response[9]<<8 | response[8])
    VIP_DIST_INPUT = response[10:16]
    VIP_BATT_INPUT = response[16:22]
    STAT_OBC_ON = (response[23]<<8 | response[22])
    STAT_OBC_OCF = (response[25]<<8 | response[24])
    BAT_STAT = (response[27]<<8 | response[26])
    BAT_TEMP2 = (response[29]<<8 | response[28])
    BAT_TEMP3 = (response[31]<<8 | response[30])
    VOLT_VD0 = (response[33]<<8 | response[32])
    VOLT_VD1 = (response[35]<<8 | response[34])
    VOLT_VD2 = (response[37]<<8 | response[36])
    VIP_OBC00 = response[38:44]
    VIP_OBC01 = response[44:50]
    VIP_OBC02 = response[50:56]
    VIP_OBC03 = response[56:62]
    VIP_OBC04 = response[62:68]
    VIP_OBC05 = response[68:74]
    VIP_OBC06 = response[74:80]
    VIP_OBC07 = response[80:86]
    VIP_OBC08 = response[86:92]
    CC1 = response[92:100]
    CC2 = response[100:108]
    CC3 = response[108:116]
    if show:
        print('Voltage of internal board supply:', VOLT_BRDSUP/1000, 'V')
        print('Measured temperature provided by a sensor internal to the MCU:', TEMP/100, '*C')
        print('Data taken at the input of the distribution part:', (VIP_DIST_INPUT[1]<<8 | VIP_DIST_INPUT[0])/1000, 'V', (VIP_DIST_INPUT[3]<<8 | VIP_DIST_INPUT[2])/1000, 'A', (VIP_DIST_INPUT[5]<<8 | VIP_DIST_INPUT[4])/100, 'W')
        print('Data taken at the input of the battery part:', (VIP_BATT_INPUT[1]<<8 | VIP_BATT_INPUT[0])/1000, 'V', (VIP_BATT_INPUT[3]<<8 | VIP_BATT_INPUT[2])/1000, 'A', (VIP_BATT_INPUT[5]<<8 | VIP_BATT_INPUT[4])/100, 'W')
        print('Channel-on status for the output bus channels:', STAT_OBC_ON)
        print('Overcurrent latch-off fault status for the output bus channels:', STAT_OBC_OCF)
        if hex(BAT_STAT) == '0x8000':
            print('BP board status:', 'battery pack enabled')
        else:
            print('BP board status:', hex(BAT_STAT))
        print('Battery pack temperature in between the center battery cells:', BAT_TEMP2/100, '*C')
        print('Battery pack temperature on the front of the battery pack:', BAT_TEMP3/100, '*C')
        print('Voltage of voltage domain 16V:', VOLT_VD0/1000, 'V')
        print('Voltage of voltage domain 5V:', VOLT_VD1/1000, 'V')
        print('Voltage of voltage domain 3.3V:', VOLT_VD2/1000, 'V')
        print('Main Rail (CH0):', (VIP_OBC00[1]<<8 | VIP_OBC00[0])/1000, 'V', (VIP_OBC00[3]<<8 | VIP_OBC00[2])/1000, 'A', (VIP_OBC00[5]<<8 | VIP_OBC00[4])/100, 'W')
        print('5V main (CH1):', (VIP_OBC01[1]<<8 | VIP_OBC01[0])/1000, 'V', (VIP_OBC01[3]<<8 | VIP_OBC01[2])/1000, 'A', (VIP_OBC01[5]<<8 | VIP_OBC01[4])/100, 'W')
        print('5V sw1 (CH2):', (VIP_OBC02[1]<<8 | VIP_OBC02[0])/1000, 'V', (VIP_OBC02[3]<<8 | VIP_OBC02[2])/1000, 'A', (VIP_OBC02[5]<<8 | VIP_OBC02[4])/100, 'W')
        print('5V sw2 (CH3):', (VIP_OBC03[1]<<8 | VIP_OBC03[0])/1000, 'V', (VIP_OBC03[3]<<8 | VIP_OBC03[2])/1000, 'A', (VIP_OBC03[5]<<8 | VIP_OBC03[4])/100, 'W')
        print('5V sw3 (CH4):', (VIP_OBC04[1]<<8 | VIP_OBC04[0])/1000, 'V', (VIP_OBC04[3]<<8 | VIP_OBC04[2])/1000, 'A', (VIP_OBC04[5]<<8 | VIP_OBC04[4])/100, 'W')
        print('3.3V main (CH5):', (VIP_OBC05[1]<<8 | VIP_OBC05[0])/1000, 'V', (VIP_OBC05[3]<<8 | VIP_OBC05[2])/1000, 'A', (VIP_OBC05[5]<<8 | VIP_OBC05[4])/100, 'W')
        print('3.3V sw1 (CH6):', (VIP_OBC06[1]<<8 | VIP_OBC06[0])/1000, 'V', (VIP_OBC06[3]<<8 | VIP_OBC06[2])/1000, 'A', (VIP_OBC06[5]<<8 | VIP_OBC06[4])/100, 'W')
        print('3.3V sw2 (CH7):', (VIP_OBC07[1]<<8 | VIP_OBC07[0])/1000, 'V', (VIP_OBC07[3]<<8 | VIP_OBC07[2])/1000, 'A', (VIP_OBC07[5]<<8 | VIP_OBC07[4])/100, 'W')
        print('3.3V sw3 (CH8):', (VIP_OBC08[1]<<8 | VIP_OBC08[0])/1000, 'V', (VIP_OBC08[3]<<8 | VIP_OBC08[2])/1000, 'A', (VIP_OBC08[5]<<8 | VIP_OBC08[4])/100, 'W')
        print('Data on conditioning chain 1')
        print('\tInput:', (CC1[1]<<8 | CC1[0])/1000, 'V', (CC1[3]<<8 | CC1[2])/1000, 'A')
        print('\tOutput:', (CC1[5]<<8 | CC1[4])/1000, 'V', (CC1[7]<<8 | CC1[6])/1000, 'A')
        print('Data on conditioning chain 2')
        print('\tInput:', (CC2[1]<<8 | CC2[0])/1000, 'V', (CC2[3]<<8 | CC2[2])/1000, 'A')
        print('\tOutput:', (CC2[5]<<8 | CC2[4])/1000, 'V', (CC2[7]<<8 | CC2[6])/1000, 'A')
        print('Data on conditioning chain 3')
        print('\tInput:', (CC3[1]<<8 | CC3[0])/1000, 'V', (CC3[3]<<8 | CC3[2])/1000, 'A')
        print('\tOutput:', (CC3[5]<<8 | CC3[4])/1000, 'V', (CC3[7]<<8 | CC3[6])/1000, 'A')
    return response

######################################
def main():
    print('\n')
    print('1: System hand shake')
    print('2: System Reset')
    print('3: Cancel Operation')
    print('4: Reset the watchdog timer')
    print('5: Turn-on output power channels')
    print('6: Turn-off output power channels')
    print('7: Switch to Nominal Mode')
    print('8: Switch to Safty Mode')
    print('9: Get system status')
    print('10: Get Housekeeping Data')
    print('00: Manual request')
    print('\n')

    cmd = input("Select: ")
    if cmd == '1':
        No_operation()
    if cmd == '2':
        System_Reset()
    if cmd == '3':
        Cancel_Operation()
    if cmd == '4':
        while True:
            try:
                Watchdog_reset()
                time.sleep(30)
            except KeyboardInterrupt:
                print('\nStop reseting')
                break
    if cmd == '5':
        channel = int(input("Which channel: "))
        Output_Bus_Channel_On(channel, True)
    if cmd == '6':
        channel = int(input("Which channel: "))
        Output_Bus_Channel_Off(channel, True)
    if cmd == '7':
        Switch_to_Nominal_Mode()
    if cmd == '8':
        Switch_to_Safety_Mode()
    if cmd == '9':
        Get_System_Status(True)
    if cmd == '10':
        Get_Housekeeping_Data(True)
    if cmd == '00':
        try:
            CC = int((input('Command address(hex):')), 16)
            size = int(input('Response size:'))
            paramin = input('Input param(list, comma delimiter):')
            paramlist = paramin.split(",")
            param = list(map(int, paramlist))
            request_data(CC, size, param)
        except Exception as e:
            print(e)
            print('Invalid input')
            pass

if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print('\nStop config EPS')
            break
