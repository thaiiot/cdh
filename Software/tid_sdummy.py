#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import serial
import os
import sys
from datetime import datetime

'''
Created on Thu June 3 2021 13:22

@author: Nuttawat Punpigul

Description: S-band dummy heartbeat check. This is for verify the Enduro's converter.
'''

# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    ms = int(now.strftime("%M"))
    return d,m,y,hr,ms

# Serial connection setup
# RS485
ser = serial.Serial('/dev/ttyUSB0', 3000000, timeout=1)  # open serial connection


# Sent request command            Receive request command ACK
print('\n---------- Start communication ----------')
while True:
    msg = str.encode('A')
    ser.write(msg)
    time.sleep(0.5)
    read = ser.readall()
    req_ack = read
    print('Sent:', msg)
    print('Recieved:', req_ack)
    time.sleep(1)
    msg = str.encode('B')
    ser.write(msg)
    time.sleep(0.5)
    read = ser.readall()
    req_ack = read
    print('Sent:', msg)
    print('Recieved:', req_ack)
    time.sleep(1)
