#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import serial
import os
import sys
import csv
import json
import str_can
from datetime import datetime
from dev_list import get_dev_list

'''
Created on Sat Sep 7 10:48:55 2019

@author: Nuttawat Punpigul

Data handling for the THAIIOT 3U cubesat payload subsystem. Using CAN bus.

TODO: - make this script running on system bootup
'''

# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    ms = int(now.strftime("%M"))
    return d,m,y,hr,ms

# get sensors data by read the csv file
def read_sensor(select_row):
    with open('/home/pi/data_handling/payload.csv', 'r+') as readfile:
        reader = csv.DictReader(readfile)
        for row in reader:
            if int(row['No.']) == select_row:
                device = "{:.2f}".format(int(row['Device'])/100)
                temp = row['Temp']
                humid = row['Humid']
                return temp, humid, device

# connect to Serial-CAN module
ser = serial.Serial(
        port ='/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 115200,
        timeout = 0 # Must set this timeout for the stable connection
)

counter = 0

while True:
    print("\n" + "Listening")
    sys.stdout.flush()  # clear the screen buffer
    while True:
        # read all CAN data being sent
        read = ser.readline()
        ser.flush()
        # read = b'\x00\x00\x00e\x01\x00\x00\x00\x00\x00\x00\x00'
        if read != b'':
            # print(read)
            break

    # convert byte array(hex) into list(decimal)
    # data format = [id1,id2,id3,data1,data2,data3,data4,data5,data6,data7,data8]
    can_parse = list(read)
    # print(bytearray(can_parse).decode("utf-8"))
    try:
        can_id = int(''.join(map(str, can_parse[0:4])))
        command = bytearray(can_parse[4:12]).decode("utf-8")
        print("ID: " + str(can_id) + '\t' + "Data: "  + command)
    except ValueError:
        continue

    # filter only attend arbitage_id
    # 100 = CDH command & respond
    # 101 = status acknowledge
    # 102 = request end devices status
    # 103 = sersor data
    try:
        if can_id == 10:
            print("Command received")

            # reset command    
            if command == 'ccprst0#':
                print("reset P/L module")
                day,month,year,hours,mins = get_datetime()
                time.sleep(1)
                print("Reset acknowledge")
                msg = str_can.encode2byte('rpcrst0#')           # send respond msg
                res_msg = [0, 0, 0x01, 0x00, 0, 0,
                           msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
                ser.write(res_msg)

                print("Reset on", str(day) + "/" + str(month) + "/" + str(year), str(hours) + ":" + str(mins))
                ack_msg = [0, 0, 0x01, 0x02, 0, 0,                # id and control field
                           0, 0, day, month, year, hours, mins, 0]         # data field
                # print(ack_msg)           
                ser.write(ack_msg)
                time.sleep(3)
                # os.system('sudo reboot')

            # shutdown command
            if command == 'ccpeof0#':
                print("shutdown")
                day,month,year,hours,mins = get_datetime()
                time.sleep(1)
                print("Shutdown acknowledge")
                msg = str_can.encode2byte('rpceof0#')           # send respond msg
                res_msg = [0, 0, 0x01, 0x00, 0, 0,
                           msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
                ser.write(res_msg) 

                print("Shutdown on", str(day) + "/" + str(month) + "/" + str(year), str(hours) + ":" + str(mins))
                ack_msg = [0, 0, 0x01, 0x02, 0, 0,                # id and control field
                           0, 1, day, month, year, hours, mins, 0]         # data field
                # print(ack_msg)           
                ser.write(ack_msg)
                time.sleep(3)
                # os.system('sudo shutdown now')
            
            # boot up command
            if command == 'ccpeon0#':
                print("boot up (wake up)")              # CDH send this to EPS for wake P/L module up

            # end-devices quantity check command
            if command == 'ccpreqn#':
                print("quantity check")
                time.sleep(0.1)
                print("processing")
                time.sleep(0.1)
                msg = str_can.encode2byte('rpcreqn#')           # send respond msg
                res_msg = [0, 0, 0x01, 0x00, 0, 0,
                                msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
                ser.write(res_msg)
                print("sending")
                row = 1
                while True:
                        try:
                                t, h, d = read_sensor(row)
                                row += 1
                        except Exception as e:
                                # print(e)
                                print(row-1)
                                res_msg = [0, 0, 0x01, 0x05, 0, 0, (row-1), 0, 0, 0, 0, 0, 0, 0]
                                ser.write(res_msg)
                                print("Done")
                                break

            # status check command
            if command == 'ccpreq1#':
                print("status check")                   # will return 0 for active else inactive.
                pkg_fwd_stat = 0
                gateway_bridge_stat = 0
                server_stat = 0
                app_server_stat = 0
                mqtt_stat = 0
                if os.system('systemctl is-active --quiet start_forwarder') != 0:
                    pkg_fwd_stat = 1
                if os.system('systemctl is-active --quiet lora-gateway-bridge') != 0:
                    gateway_bridge_stat = 1
                if os.system('systemctl is-active --quiet loraserver') != 0:
                    server_stat = 1
                if os.system('systemctl is-active --quiet lora-app-server') != 0:
                    app_server_stat = 1
                if os.system('systemctl is-active --quiet mosquitto') != 0:
                    mqtt_stat = 1
                print("Package forwarder: ", pkg_fwd_stat)
                print("Gateway bridge: ", gateway_bridge_stat)
                print("Server: ", server_stat)
                print("AppServer: ", app_server_stat)
                print("MQTT: ", mqtt_stat)
                msg = str_can.encode2byte('rpcreq1#')           # send respond msg
                res_msg = [0, 0, 0x01, 0x00, 0, 0,
                           msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
                ser.write(res_msg)
                # time.sleep(1)
                ack_msg = [0, 0, 0x01, 0x01, 0, 0,
                           pkg_fwd_stat, gateway_bridge_stat, server_stat, mqtt_stat, 0, 0, 0, 0]
                ser.write(ack_msg)
                # time.sleep(1)

            # request device status
            if command == 'ccpreq2#':
                print("request device status")
                time.sleep(1)
                print("processing")

                # get_dev_list()
                time.sleep(1)
                msg = str_can.encode2byte('rpcreq2#')           # send respond msg
                res_msg = [0, 0, 0x01, 0x00, 0, 0,
                           msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
                # print(res_msg)
                ser.write(res_msg)
                print("sending")
                # get device list by read a json file
                with open('/home/pi/data_handling/devices.json', 'r') as dev_file:
                    dev_list = json.loads(dev_file.read())
                num = 0
                while True:
                    try:
                        dev_num = str(int(dev_list['device'][num]['device_num'])/100).split('.')
                        if dev_list['device'][num]['activation'] == 'True':
                            activation = 1
                        else:
                            activation = 2

                        dev_list_msg = [0, 0, 0x01, 0x03, 0, 0,
                                        int(dev_num[0]), int(dev_num[1]), activation, 
                                        0, 0, 0, 0, 0]
                        # print(dev_list_msg)
                        ser.write(dev_list_msg)
                        print("device no. ", (dev_num[0] + dev_num[1]), "sent")
                        # time.sleep(0.1)
                        num += 1
                    except Exception as e:
                        # print(e)
                        print("Done")
                        break
                time.sleep(3)

            # request sensor data
            if command == 'ccpreq3#':
                print("request sensor data")
                time.sleep(1)
                print("processing")
                time.sleep(1)
                msg = str_can.encode2byte('rpcreq3#')           # send respond msg
                res_msg = [0, 0, 0x01, 0x00, 0, 0,
                           msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]]
                ser.write(res_msg)
                print("sending")
                row = 1
                while True:
                    try:
                        t, h, d = read_sensor(row)
                        dev_num = str(d).split('.')
                        temp = str(t).split('.')
                        humid = str(h).split('.')
                        sensor_msg = [  0,                  # id1
                                        0,                  # id2
                                        0x01,               # id3
                                        0x04,               # id4
                                        0,                  # ext
                                        0,                  # rtr
                                        int(dev_num[0]),    # device number high byte
                                        int(dev_num[1]),    # device number low byte
                                        int(temp[0]),       # sensor1 high byte
                                        int(temp[1]),       # sensor1 low byte
                                        int(humid[0]),      # sensor2 high byte
                                        int(humid[1]),      # sensor2 low byte
                                        0, 0
                                    ]
                        # print(sensor_msg)
                        ser.write(sensor_msg)
                        print("device no. ", row, "sent")
                        # time.sleep(0.1)
                        row += 1
                    except Exception as e:
                        # print(e)
                        print("Done")
                        break
                time.sleep(3)

            # reserve command bit
            if can_parse[10] == 1:
                print("Byte not set")
            if can_parse[11] == 1:
                print("Byte not set")
    except Exception as e:
        # print("Msg reading error")
        print("Error:", e)
    # time.sleep(30)