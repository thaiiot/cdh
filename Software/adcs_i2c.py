#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
from datetime import datetime
from smbus2 import SMBus, i2c_msg

'''
Created on Sat Sep 14 8:48 2020

@author: Nuttawat Punpigul

Command fuction of the Attitude Determination and Control subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1

i2c_addr = 0x57  # EPS I2C address

# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    mn = int(now.strftime("%M"))
    return d,m,y,hr,mn

def setmode(mode='SLEEP'):
    cur_mode = mode


def getmode():
    d,m,y,hr,mn = get_datetime()
    if hr == 10:
        cur_mode = 'NADIR'
    else:
        cur_mode = 'SLEEP'
    return cur_mode

#TODO: NADIR status check >>> measuring power >>> TX

def request_data(reg_addr, size, show = False):
    data = [reg_addr]
    time.sleep(0.1)
    write = i2c_msg.write(i2c_addr, data)
    bus.i2c_rdwr(write)
    read = i2c_msg.read(i2c_addr, size)
    bus.i2c_rdwr(read)
    data_buf = list(read)
    if show:
        print("request data")
        # print('Got =', data_buf)
        for i in range(size):
            print(list(map(int, f'{data_buf[i]:08b}')))
    return data_buf


def command(tc_id, param = [], show = False):
    data = [tc_id] + param
    time.sleep(0.1)
    write = i2c_msg.write(i2c_addr, data)
    bus.i2c_rdwr(write)
    if show:
        print("Command:", end='')
        print(list(write))
    tele_ack(True)

def tele_ack(show = False):
    data = request_data(0xF0, 4)
    if data[1] != 1:
        data = request_data(0xF0, 4)
    if show:
        print("\nACK")
        # print(data)
        print('Last TLC ID:', data[0])
    if data[2] != 0:
        if data[2] == 1:
            err_stat = 'Invalid TC'
        if data[2] == 2:
            err_stat = 'Incorrect Length '
        if data[2] == 3:
            err_stat = 'Incorrect Parameter'
        if data[2] == 4:
            err_stat = 'CRC check failed'
        print('\nERROR:', err_stat, '\n')
        return
    # print(list(map(int, f'{data[3]:08b}')))
    print('\nSuccessed\n')

def ADCS_Triggered_Mode():
    command(10,[2])
    time.sleep(0.5)
    command(19,[])

def main():
    pass
    # command(0x0B,[5,0,0])
    # request_data(128, 6, True)


if __name__ == "__main__":
    # while True:
        # try:
            main()
        # except KeyboardInterrupt:
        #     print('\nStop config ADCS')
        #     break
