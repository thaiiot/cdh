import time, socket, random, serial

# Serial connection setup
ser = serial.Serial('/dev/ttyS0', 115200, timeout=0.1)  # open serial connection
# print(ser.isOpen)     #  check the serial connection
# ser.open()
print('Open serial on', ser.name)    #  display port name

STID = 0x1A  # System Type Identifier (ICEPS)
IVID = 0x05  # Interface Version Identifier
BID = 0x01	 # Board Identifier

while True:
    print('\n')
    print('1: System hand shake')
    print('2: System Reset')
    print('3: Cancel Operation')
    print('4: Reset the watchdog timer')
    print('5: Turn-on output power channels')
    print('6: Turn-off output power channels')
    print('7: Switch to Nominal Mode')
    print('8: Switch to Safty Mode')
    print('9: Get system status')
    print('10: Get Housekeeping Data')
    print('00: Manual request')
    print('\n')

    param = []
    cmd = input("Select: ")
    if cmd == '4':
        CMD_ID = 0x06
    if cmd == '5':
        param = [int(input("Which channel: "))]
        CMD_ID = 0x16
    if cmd == '6':
        param = [int(input("Which channel: "))]
        CMD_ID = 0x18
    if cmd == '9':
        CMD_ID = 0x40
    if cmd == '10':
        CMD_ID = 0xA2

    data = [0x3C, 0x63, 0x6D, 0x64, 0x3E] + [STID] + [IVID] + [CMD_ID] + [BID] + param + [0x3C, 0x2F, 0x63, 0x6D, 0x64, 0x3E]
    ser.flushInput() #flush input buffer, discarding all its contents
    ser.flushOutput() #flush output buffer, aborting current output and discard all that is in buffer
    time.sleep(0.1)
    ser.write(bytes(data))
    time.sleep(0.1)
    read = ser.readall()
    data_buf = list(read)
    # print("Reponse data:")
    # print(data_buf)

    if CMD_ID == 0x40:
        print("\n(0x40) Get System Status")
        print('')
        print('STAT = ', hex(data_buf[9]))
        mode = data_buf[10]
        if mode == 0:
            print('Mode: STARTUP')
        if mode == 1:
            print('Mode: NOMINAL')
        if mode == 2:
            print('Mode: SAFTY')
        if mode == 3:
            print('Mode: EMERGENCY LOW POWER')
        conf = data_buf[11]
        if conf == 0:
            print('Read/write configuration parameters have been changed: no')
        if conf == 1:
            print('Read/write configuration parameters have been changed: yes')
        reset_cause = data_buf[12]
        if reset_cause == 0:
            print('Cause of last reset: power-on')
        if reset_cause == 1:
            print('Cause of last reset: watchdog')
        if reset_cause == 2:
            print('Cause of last reset: commanded')
        if reset_cause == 3:
            print('Cause of last reset: control system reset')
        if reset_cause == 4:
            print('Cause of last reset: emlopo')
        uptime = (data_buf[16]<<8 | data_buf[15]<<8 | data_buf[14]<<8 | data_buf[13])
        print('System uptime:', uptime, 'second')
        error = (data_buf[18]<<8 | data_buf[17])
        print('First internal error encountered during the system control cycle:', error)
        RC_CNT_PWRON = (data_buf[20]<<8 | data_buf[19])
        print('Power-on reset counter:', RC_CNT_PWRON)
        RC_CNT_WDG = (data_buf[22]<<8 | data_buf[21])
        print('Watchdog reset counter:', RC_CNT_WDG)
        RC_CNT_CMD = (data_buf[24]<<8 | data_buf[23])
        print('Commanded reset counter:', RC_CNT_CMD)
        RC_CNT_MCU = (data_buf[26]<<8 | data_buf[25])
        print('EPS controller reset counter:', RC_CNT_MCU)
        RC_CNT_EMLOPO = (data_buf[28]<<8 | data_buf[27])
        print('Emergency low power reset counter:', RC_CNT_EMLOPO)
        PREVCMD_ELAPSED = (data_buf[30]<<8 | data_buf[29])
        print('Time elapsed between reception of the previous and this command:', PREVCMD_ELAPSED,'second')
        UNIX_TIME = (data_buf[34]<<8 | data_buf[33]<<8 | data_buf[32]<<8 | data_buf[31])
        print('UNIX time:', UNIX_TIME)

    if CMD_ID == 0xA2:
        print("\n(0xA2) Get PIU Housekeeping Data (ENG)")
        print('')
        print('STAT = ', hex(data_buf[9]))
        VOLT_BRDSUP = (data_buf[12]<<8 | data_buf[11])
        TEMP = (data_buf[14]<<8 | data_buf[13])
        VIP_DIST_INPUT = data_buf[15:21]
        VIP_BATT_INPUT = data_buf[21:27]
        STAT_OBC_ON = (data_buf[28]<<8 | data_buf[27])
        STAT_OBC_OCF = (data_buf[30]<<8 | data_buf[29])
        BAT_STAT = (data_buf[32]<<8 | data_buf[31])
        BAT_TEMP2 = (data_buf[34]<<8 | data_buf[33])
        BAT_TEMP3 = (data_buf[36]<<8 | data_buf[35])
        VOLT_VD0 = (data_buf[38]<<8 | data_buf[37])
        VOLT_VD1 = (data_buf[40]<<8 | data_buf[39])
        VOLT_VD2 = (data_buf[42]<<8 | data_buf[41])
        VIP_OBC00 = data_buf[43:49]
        VIP_OBC01 = data_buf[49:55]
        VIP_OBC02 = data_buf[55:61]
        VIP_OBC03 = data_buf[61:67]
        VIP_OBC04 = data_buf[67:73]
        VIP_OBC05 = data_buf[73:79]
        VIP_OBC06 = data_buf[79:85]
        VIP_OBC07 = data_buf[85:91]
        VIP_OBC08 = data_buf[91:97]
        CC1 = data_buf[97:105]
        CC2 = data_buf[105:113]
        CC3 = data_buf[113:121]

        print('Voltage of internal board supply:', VOLT_BRDSUP/1000, 'V')
        print('Measured temperature provided by a sensor internal to the MCU:', TEMP/100, '*C')
        print('Data taken at the input of the distribution part:', (VIP_DIST_INPUT[1]<<8 | VIP_DIST_INPUT[0])/1000, 'V', (VIP_DIST_INPUT[3]<<8 | VIP_DIST_INPUT[2])/1000, 'A', (VIP_DIST_INPUT[5]<<8 | VIP_DIST_INPUT[4])/100, 'W')
        print('Data taken at the input of the battery part:', (VIP_BATT_INPUT[1]<<8 | VIP_BATT_INPUT[0])/1000, 'V', (VIP_BATT_INPUT[3]<<8 | VIP_BATT_INPUT[2])/1000, 'A', (VIP_BATT_INPUT[5]<<8 | VIP_BATT_INPUT[4])/100, 'W')
        print('Channel-on status for the output bus channels:', STAT_OBC_ON)
        print('Overcurrent latch-off fault status for the output bus channels:', STAT_OBC_OCF)
        if hex(BAT_STAT) == '0x8000':
            print('BP board status:', 'battery pack enabled')
        else:
            print('BP board status:', hex(BAT_STAT))
        print('Battery pack temperature in between the center battery cells:', BAT_TEMP2/100, '*C')
        print('Battery pack temperature on the front of the battery pack:', BAT_TEMP3/100, '*C')
        print('Voltage of voltage domain 16V:', VOLT_VD0/1000, 'V')
        print('Voltage of voltage domain 5V:', VOLT_VD1/1000, 'V')
        print('Voltage of voltage domain 3.3V:', VOLT_VD2/1000, 'V')
        print('Main Rail (CH0):', (VIP_OBC00[1]<<8 | VIP_OBC00[0])/1000, 'V', (VIP_OBC00[3]<<8 | VIP_OBC00[2])/1000, 'A', (VIP_OBC00[5]<<8 | VIP_OBC00[4])/100, 'W')
        print('5V main (CH1):', (VIP_OBC01[1]<<8 | VIP_OBC01[0])/1000, 'V', (VIP_OBC01[3]<<8 | VIP_OBC01[2])/1000, 'A', (VIP_OBC01[5]<<8 | VIP_OBC01[4])/100, 'W')
        print('5V sw1 (CH2):', (VIP_OBC02[1]<<8 | VIP_OBC02[0])/1000, 'V', (VIP_OBC02[3]<<8 | VIP_OBC02[2])/1000, 'A', (VIP_OBC02[5]<<8 | VIP_OBC02[4])/100, 'W')
        print('5V sw2 (CH3):', (VIP_OBC03[1]<<8 | VIP_OBC03[0])/1000, 'V', (VIP_OBC03[3]<<8 | VIP_OBC03[2])/1000, 'A', (VIP_OBC03[5]<<8 | VIP_OBC03[4])/100, 'W')
        print('5V sw3 (CH4):', (VIP_OBC04[1]<<8 | VIP_OBC04[0])/1000, 'V', (VIP_OBC04[3]<<8 | VIP_OBC04[2])/1000, 'A', (VIP_OBC04[5]<<8 | VIP_OBC04[4])/100, 'W')
        print('3.3V main (CH5):', (VIP_OBC05[1]<<8 | VIP_OBC05[0])/1000, 'V', (VIP_OBC05[3]<<8 | VIP_OBC05[2])/1000, 'A', (VIP_OBC05[5]<<8 | VIP_OBC05[4])/100, 'W')
        print('3.3V sw1 (CH6):', (VIP_OBC06[1]<<8 | VIP_OBC06[0])/1000, 'V', (VIP_OBC06[3]<<8 | VIP_OBC06[2])/1000, 'A', (VIP_OBC06[5]<<8 | VIP_OBC06[4])/100, 'W')
        print('3.3V sw2 (CH7):', (VIP_OBC07[1]<<8 | VIP_OBC07[0])/1000, 'V', (VIP_OBC07[3]<<8 | VIP_OBC07[2])/1000, 'A', (VIP_OBC07[5]<<8 | VIP_OBC07[4])/100, 'W')
        print('3.3V sw3 (CH8):', (VIP_OBC08[1]<<8 | VIP_OBC08[0])/1000, 'V', (VIP_OBC08[3]<<8 | VIP_OBC08[2])/1000, 'A', (VIP_OBC08[5]<<8 | VIP_OBC08[4])/100, 'W')
        print('Data on conditioning chain 1')
        print('\tInput:', (CC1[1]<<8 | CC1[0])/1000, 'V', (CC1[3]<<8 | CC1[2])/1000, 'A')
        print('\tOutput:', (CC1[5]<<8 | CC1[4])/1000, 'V', (CC1[7]<<8 | CC1[6])/1000, 'A')
        print('Data on conditioning chain 2')
        print('\tInput:', (CC2[1]<<8 | CC2[0])/1000, 'V', (CC2[3]<<8 | CC2[2])/1000, 'A')
        print('\tOutput:', (CC2[5]<<8 | CC2[4])/1000, 'V', (CC2[7]<<8 | CC2[6])/1000, 'A')
        print('Data on conditioning chain 3')
        print('\tInput:', (CC3[1]<<8 | CC3[0])/1000, 'V', (CC3[3]<<8 | CC3[2])/1000, 'A')
        print('\tOutput:', (CC3[5]<<8 | CC3[4])/1000, 'V', (CC3[7]<<8 | CC3[6])/1000, 'A')

    if CMD_ID == 0x06:
        while True:
            ser.write(bytes(data))
            print('\n(0x06)Reset the watchdog timer')
            print('Write data:')
            print(data)
            time.sleep(1)
            print("Reponse data:")
            print(data_buf)
            ser.flushInput()
            ser.flushOutput()
            time.sleep(30)

    if CMD_ID == 0x16:
        print("\n(0x16) Turn-on output power channels")
        print('')
        print('STAT = ', hex(data_buf[9]))

    if CMD_ID == 0x18:
        print("\n(0x18) Turn-off output power channels")
        print('')
        print('STAT = ', hex(data_buf[9]))

    time.sleep(0.1)
    print('Write data:')
    print(data)
    time.sleep(1)
    print("Reponse data:")
    print(data_buf)
    time.sleep(2)