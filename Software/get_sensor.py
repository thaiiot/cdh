#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import time
import json
import csv
import sys
import os
import requests
from datetime import datetime

'''
Created on Sat Sep 07 09:30:42 2019

@author: Nuttawat Punpigul

Subscribe to uplink MQTT topic to get end devices sensor data and store them in a file.
TODO: - add connection checking
      - save file name "(date)"
      - **make this script running on system bootup**
'''

'''
LoRa server MQTT topic template

uplink_topic_template   = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/rx"
downlink_topic_template = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/tx"
join_topic_template     = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/join"
ack_topic_template      = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/ack"
error_topic_template    = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/error"
status_topic_template   = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/status"
location_topic_template = "application/{{ .ApplicationID }}/device/{{ .DevEUI }}/location"
'''
# global variable 
counter = 1

# get current date and time function
def get_datetime():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    return d,m,y

def get_dev_type(dev_eui):
    # API url (change the ip address to the App server ip)
    url = 'http://127.0.0.1:8080/api/devices'

    # get jwt token by read 'jwt.txt' file
    with open('/home/pi/data_handling/jwt.json', 'r') as jwt_file:
        jwt = jwt_file.read()

    headers = {
                'Accept': 'application/json',
                'Grpc-Metadata-Authorization': 'Bearer ' + jwt,
                }

    # search parameter (how much devices will be get, ID num of application)
    params = (
        ('limit', '10'),
        ('search', str(dev_eui)),
    )

    # request devices list
    response = requests.get(url, headers=headers, params=params)
    response = json.loads(json.dumps(response.json()))
    # print(response['result'])
    # print(json.dumps(response.json(), indent=4))
    dev_type = response['result'][0]['deviceProfileName']
    return dev_type
    

# Write wp function
def write_file(time_stamp, dev_num, data1, data2, dev_type):
    d,m,y = get_datetime()
    date = "{:02d}".format(d) + "{:02d}".format(m) + str(y)
    filename = '/home/pi/data_handling/' + date + '.csv'
    header = [["Time", "Device", "Data1", "Data2", "Type"]]
    row = [[str(time_stamp), str(dev_num), str(data1), str(data2), str(dev_type)]]
    if os.path.exists(filename):
        with open(filename, 'a', newline ='') as writeFile:
            writer = csv.writer(writeFile)
            writer.writerows(row)
        writeFile.close()
    else:
        with open(filename, 'w', newline ='') as writeFile:
            writer = csv.writer(writeFile)
            writer.writerows(header)
            writer.writerows(row)
        writeFile.close()

def on_message(client, userdata, message):
    # print("message received " ,str(message.payload.decode("utf-8")))
    print("message received ")
    data = json.loads(message.payload.decode("utf-8"))         # convert byte array to json object
    time_stamp = datetime.now().strftime("%H:%M:%S")
    dev_num = data['deviceName']
    dev_eui = data['devEUI']
    data1 = data['object']['data1']
    data2 = data['object']['data2']
    print(json.dumps(data, indent=4))
    print("message topic=",message.topic)
    dev_type = get_dev_type(dev_eui)
    write_file(time_stamp, dev_num, data1, data2, dev_type)
    print("Written into a file\n")


broker_address="127.0.0.1"        # MQTT broker ip address
topic = "application/+/device/+/rx"     # topic for subscribing
# print("creating new instance")
client = mqtt.Client("P1")              # create new instance
client.on_message = on_message        # attach function to callback
print("connecting to broker", broker_address)
client.connect(broker_address)          # connect to broker
print("Subscribing to topic", topic)

while True:
    client.loop_start()                 # start the loop
    client.subscribe(topic)             # subscribing
    # client.publish("topic","msg")
    time.sleep(1)                       # wait
    # client.loop_stop()                #stop the loop
