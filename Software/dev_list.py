#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import json

'''
Created on Sat Sep 07 09:30:42 2019

@author: Nuttawat Punpigul

Description: Get the end devices data by using stored JWT token
The script will create a json file to store the generated JWT token.

Convert from CURL code to python code
TODO: - add more parameter for device check
'''
def get_dev_list():
  # API url (change the ip address to the App server ip)
  url = 'http://127.0.0.1:8080/api/devices'

  # get jwt token by read 'jwt.txt' file
  with open('jwt.json', 'r') as jwt_file:
    jwt = jwt_file.read()

  headers = {
              'Accept': 'application/json',
              'Grpc-Metadata-Authorization': 'Bearer ' + jwt,
            }

  # search parameter (how much devices will be get, ID num of application)
  params = (
      ('limit', '100'),
  )

  # request devices list
  response = requests.get(url, headers=headers, params=params)
  response = json.loads(json.dumps(response.json()))
  print("Devices list received")
  # print(response['result'])
  # print(json.dumps(response.json(), indent=4))

  dev_list = {}
  dev_list["device"] = []
  num = 0
  print("Saved in file")
  while True:
    try:
      name_object = response['result'][num]['name']
      dev_list['device'].insert(num, {})
      dev_list['device'][num]["device_num"] = name_object
      if response['result'][num]['lastSeenAt'] == None:
        dev_list["device"][num]["activation"] = "False"
      else:
        dev_list["device"][num]["activation"] = "True"
      num += 1
    except:
      with open('home/pi/data_handling/devices.json', 'w') as outfile:
        list_write = json.dumps(dev_list)
        outfile.write(list_write)
      break
    
    
if __name__ == '__main__':
  get_dev_list()
