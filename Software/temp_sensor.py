import time
import board
import math
import io
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
from statistics import mean
 
i2c_bus = board.I2C()
ads = ADS.ADS1115(i2c_bus)
# ads.gain = 8

# Thermistor configuration values
THERMISTORNOMINAL = 10000  # resistance at 25 degrees C
TEMPERATURENOMINAL = 25    # temp. for nominal resistance (almost always 25 C)
SERIESRESISTOR = 10000     # the value of the 'fixed' resistor

def converted_to_temp(Vout):
    if Vout == 0:
        TempC = 0
        return TempC
    Vin = 3.3               # voltage reference
    Ro = 10000              # the value of the 'fixed' resistor
    A = 0.001129148         # stainhart constant
    B = 0.000234125
    C = 0.0000000876741
    Rt = (Vout * Ro) / (Vin - Vout)
    # print('Resistance {:6.3f} Ohm'.format(Rt))
    TempK = 1 / (A + (B * math.log(Rt)) + C * math.pow(math.log(Rt),3))
    # print('TempK {:6.3f} K'.format(TempK))
    TempC = TempK - 273.15
    # print('TempC {:6.3f} C'.format(TempC))
    return TempC

while True:
    # sample analog values 'NUMSAMPLES' times to make average values of each channel
    ch0 = (AnalogIn(ads, ADS.P0)).voltage
    ch1 = (AnalogIn(ads, ADS.P1)).voltage
    ch2 = (AnalogIn(ads, ADS.P2)).voltage
    ch3 = (AnalogIn(ads, ADS.P3)).voltage

    # print('SX1255_1    ', (AnalogIn(ads, ADS.P0)).value, (AnalogIn(ads, ADS.P0)).voltage)
    # print('SX1255_2    ', (AnalogIn(ads, ADS.P1)).value, (AnalogIn(ads, ADS.P1)).voltage)
    # print('SX1301      ', (AnalogIn(ads, ADS.P2)).value, (AnalogIn(ads, ADS.P2)).voltage)
    # print('Ambient temp', (AnalogIn(ads, ADS.P3)).value, (AnalogIn(ads, ADS.P3)).voltage)
    # print('')
    print('SX1255_1 {:6.3f} C'.format(converted_to_temp(ch0)))
    print('SX1255_2 {:6.3f} C'.format(converted_to_temp(ch1)))
    print('SX1301   {:6.3f} C'.format(converted_to_temp(ch2)))
    print('Ambient temp {:6.3f} C'.format(converted_to_temp(ch3)))

    f = open("/sys/class/thermal/thermal_zone0/temp", "r")
    t = int(f.readline ()) / 1000
    print ("CPU temp:" , t, 'C')

    print('')
    
    time.sleep(0.5)