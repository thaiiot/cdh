#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import csv
import os
import eps_i2c
import commu_i2c
import adcs_i2c
import ant_deploy
import enduro
# import pay_i2c
from datetime import datetime
# from smbus2 import SMBus, i2c_msg

'''
Created on Tue Jul 21 12:51 2020

@author: Nuttawat Punpigul

Flight software for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''
# I2C address
uv_addr = 0x31  # UV I2C address
lora_addr = 0x33  # LoRa I2C address
adcs_addr = 0x57  # ADCS I2C address

# EPS init mode
eps_mode = 0

# get current date and time function
def get_datetime ():
    now = datetime.now()
    d = int(now.strftime("%d"))
    m = int(now.strftime("%m"))
    y = int(now.strftime("%Y")) - 2000
    hr = int(now.strftime("%H"))
    mn = int(now.strftime("%M"))
    return d,m,y,hr,mn

def enough_power(min_v=1500):
    batt_vipd = eps_i2c.request_data(0xA2, 116)
    voltage_b = [batt_vipd[17], batt_vipd[16]]
    voltage = (batt_vipd[17]<<8 | batt_vipd[16]) #1e-3 V
    print('Remaining voltage:', round((voltage * 1e-3), 3), 'V')
    if voltage > min_v:
        go_nogo = True
    else:
        go_nogo = False
    return go_nogo, voltage_b

def not_enough_power(batt_vipd):
    print('Not enough Voltage')
    time.sleep(1)
    print('Send warning msg to GCS')
    txt_list = list(map(ord, list("Not Enough Voltage")))
    data = batt_vipd + txt_list
    commu_i2c.Send_frame(data)
    time.sleep(1)

def eps_status(eps_mode):
    mode = eps_i2c.Get_System_Status()
    if mode != eps_mode:
        if mode == 0:
            print('EPS Mode: STARTUP')
        if mode == 1:
            print('EPS Mode: NOMINAL')
            eps_i2c.Output_Bus_Channel_On(2) # PAY power ON
            time.sleep(0.1)
            eps_i2c.Output_Bus_Channel_On(6) # ADCS power ON
            time.sleep(0.1)
        if mode == 2:
            print('EPS Mode: LOW POWER')
            time.sleep(1)
            print('PAY: OFF')
            eps_i2c.Output_Bus_Channel_Off(2)
            time.sleep(1)
            print('ADCS: OFF')
            eps_i2c.Output_Bus_Channel_Off(6)
            time.sleep(1)
            print('S-TX: OFF')
            eps_i2c.Output_Bus_Channel_Off(3)
            time.sleep(1)
            print('Beacon interval change (120s)')
            commu_i2c.Set_beacon(120) # FIXME: beacon interval
            time.sleep(0.5)
            print('Beacon started')
            print('30s timeout')
            time.sleep(30)
        if mode == 3:
            print('EPS Mode: EMERGENCY LOW POWER')
            time.sleep(1)
            print('PAY: OFF')
            eps_i2c.Output_Bus_Channel_Off(6)
            time.sleep(1)
            print('ADCS: OFF')
            eps_i2c.Output_Bus_Channel_Off(2)
            time.sleep(1)
            print('S-TX: OFF')
            eps_i2c.Output_Bus_Channel_Off(3)
            time.sleep(1)
            print('Beacon interval change (300s)')
            commu_i2c.Set_beacon(300) # FIXME: beacon interval
            time.sleep(0.5)
            print('Beacon started')
            print('60s timeout')
            time.sleep(60)

def main():
    time.sleep(3)
    clear()
    time.sleep(0.5)
    print('< Start tx beacon >')
    commu_i2c.Set_beacon() # FIXME: beacon interval
    time.sleep(0.5)
    print('Beacon started')
    time.sleep(1)
    # print('\n< ADCS status >')
    # # TODO: ADCS I2C protocol integration (Standalone service)
    # time.sleep(1)
    # print('ADCS Mode: STANDBY')
    # # print('Attitude ---> vx/vy/vz, wx/wy/wz')
    # # print('Velocity ---> m/s')
    # # print('Height ---> m')
    # time.sleep(1)
    # print('\n< EPS status >')
    # eps_status()
    # time.sleep(1)
    # enough_power()
    # time.sleep(3)

    while True:
        clear()
        time.sleep(0.3)
        print('\n>>> Satellite Standby mode <<<')
        print('\n< Time&Location >')
        # TODO: GPS & RTC I2C protocol integration
        d,m,y,hr,mn = get_datetime()
        print('Time:', d, '/', m, '/', y, '\t', hr + 6, ':', mn, 'UTC+7')
        print('Lat:', '13.920280', 'Lon:', '100.627625', '\n')
        # time.sleep(1)
        if hr == 10:
            if adcs_i2c.getmode() != 'NADIR': # FIXME: write proper i2c code
                print('Preparing for NADIR pointing ---> Measure remaining power')
                go_nogo, batt_vipd = enough_power(15000)
                time.sleep(1)
                if go_nogo:
                    print('Enough power')
                    time.sleep(1)
                    print('Begin NADIA pointing...') # TODO:
                    time.sleep(1)
                else:
                    not_enough_power(batt_vipd)
                    print('Back to Standby mode')
                    time.sleep(1)
                    commu_i2c.Set_beacon() # FIXME: beacon interval
                    time.sleep(0.5)
                    print('Beacon started')
                    time.sleep(1)
                    pass
            print('ADCS: NADIR pointing mode')
        else:
            print('ADCS: STANDBY mode') # TODO:
        
        eps_status(eps_mode)
        enough_power()

        print('\nCheck COMM RX buffer')
        rx_buf = commu_i2c.Get_number_of_frames_in_receive_buffer() # FIXME:uncomment
        # rx_buf = 1
        if rx_buf > 0:
            print('Got a message')
            msg = commu_i2c.Get_frame_from_receive_buffer() # FIXME:uncomment
            # msg = input('Input command: ')
            # TODO: parse msg to command
            # Telemetry request
            # Temp all sub
            # VIPD all sub
            #
            if msg == 'tele_commu':
                print('Telemetry request for COMMU')
                time.sleep(0.5)
                data = [0xFF] + commu_i2c.Measure_all_the_telemetry_channels() + commu_i2c.Report_transmitter_state() + [0xFF]
                time.sleep(0.5)
                print('Send telemetry response')
                commu_i2c.Send_frame(data)
                time.sleep(5)
                print('\nBack to Standby mode')
                break
            if msg == 'tele_eps':
                print('Telemetry request for EPS')
                # Get_System_Status
                status1 = eps_i2c.request_data(0x40, 36)
                status2 = eps_i2c.Get_Housekeeping_Data()
                time.sleep(0.5)
                data = [0xFF] + status1[5:] + status2[6:] + [0xFF]
                time.sleep(0.5)
                print('Send telemetry response')
                commu_i2c.Send_frame(data)
                time.sleep(5)
                print('\nBack to Standby mode')
                break
            if msg == 'tele_stx':
                print('Telemetry request for STX')
                sys_state = enduro.GET([0x49, 0x00])
                all_param = enduro.GET([0x48, 0x00])
                file_src = enduro.GET([0x4D, 0x00])
                time.sleep(0.5)
                data = [0xFF] + sys_state[15:] + all_param[15:] + file_src[15:] + [0xFF]
                time.sleep(0.5)
                print('Send telemetry response')
                commu_i2c.Send_frame(data)
                time.sleep(5)
                print('\nBack to Standby mode')
                break
            if msg == 'tele_adcs':
                print('Telemetry request for ADCS')
                time.sleep(0.5)
                data = [0xFF] + [] + [0xFF]
                time.sleep(0.5)
                print('Send telemetry response')
                commu_i2c.Send_frame(data)
                time.sleep(5)
                print('\nBack to Standby mode')
                break
            if msg == 'tele_pay':
                print('Telemetry request for PAY')
                time.sleep(0.5)
                data = [0xFF] + [] + [0xFF]
                time.sleep(0.5)
                print('Send telemetry response')
                commu_i2c.Send_frame(data)
                time.sleep(5)
                print('\nBack to Standby mode')
                break
            if msg == 'tele_cdh':
                print('Telemetry request for CDH')
                time.sleep(0.5)
                data = [0xFF] + [] + [0xFF]
                time.sleep(0.5)
                print('Send telemetry response')
                commu_i2c.Send_frame(data)
                time.sleep(5)
                print('\nBack to Standby mode')
                break
            # Mission mode
            if msg == 'mission':
                print('Mission mode')
                print('Turn off beacon')
                commu_i2c.Clear_beacon()
                time.sleep(1)
                print('Measure remaining power')
                go_nogo, batt_vipd = enough_power(14500)
                time.sleep(1)
                if go_nogo:
                    print('Enough power')
                    time.sleep(1)
                    while True:
                        for retry in range(3):
                            try:
                                print('\nTurn on LoRa gateway')
                                os.system('systemctl restart start_forwarder.service')
                                time.sleep(3)
                                if os.system('systemctl is-active --quiet start_forwarder') == 0:
                                    print('LoRa gateway started')
                                    print('Check the current drawned by LoRa gateway')
                                    # TODO: measure current draw on 3V3(OBC1) to check the gateway status
                                    # if 3v3_obc1_i > 0.01: True, else retry
                                    time.sleep(20) # FIXME: 10 mins
                                    print('Mission timeout! ---> Turn off LoRa')
                                    os.system('systemctl stop start_forwarder.service')
                                    print('Check the current drawned by LoRa gateway')
                                    # TODO:
                                    time.sleep(1)
                                    print('\nBack to Standby mode')
                                    break
                                print('Something wrong, attemped:', retry)
                                time.sleep(10)
                            except:
                                continue
                        if retry >= 2:
                            print('Failed to start the LoRa gateway')
                            print('Send warning msg to GCS')
                            txt_list = list(map(ord, list("Failed to start the LoRa gateway")))
                            commu_i2c.Send_frame(txt_list)
                            print('Back to Standby mode')
                            break
                        break
                else:
                    not_enough_power(batt_vipd)
                    print('Back to Standby mode')
                    time.sleep(1)
            # S-band
            if msg == 'strx':
                print('S transmission mode')
                # print('Turn off beacon')
                # commu_i2c.Clear_beacon()
                time.sleep(1)
                print('Measure remaining power')
                time.sleep(1)
                go_nogo, batt_vipd = enough_power(14500)
                if go_nogo:
                    print('Enough Voltage')
                    time.sleep(1)
                    print('Start S-band transmission')
                    # TODO: S-band I2C protocol integration
                    time.sleep(1)
                    print('Done')
                    time.sleep(1)
                    print('Back to Standby mode')
                    break
                else:
                    not_enough_power(batt_vipd)
                    print('Back to Standby mode')
                    time.sleep(1)
                    break
            else:
                print('Invalid command')
        else:
            print('No message')

        time.sleep(10)

# bus = SMBus(1) # indicates /dev/ic2-1
path_to_file = '/home/pi/data_handling/init_timeout.txt'
clear = lambda: os.system("clear")

## Deployment phase
# check that the satellite already wait for post-deployment regulation or not
print('Initializing...')
time.sleep(1)
f=open(os.path.join(path_to_file), 'r+')
if f.mode == 'r+':
    contents = f.read()
if contents == '0':
    print('Begin Post-deployment procedure')
    print('Wait for 30 mins')
    time.sleep(10)    # 30 minutes post-deployment regulation FIXME:this delay time is for testing
    f=open(os.path.join(path_to_file), 'w+')
    f.write('1')
    f.close()
    print('Post-deployment timeout')
    time.sleep(1)
    print('\nInitiate antenna deployment procedure')
    time.sleep(1)
    print('Deploy UV antenna')
    ant_deploy.Start_deploy(uv_addr)
    time.sleep(1)
    print('Deploy LoRa antenna')
    ant_deploy.Start_deploy(lora_addr)
    time.sleep(1)
    print('Check the antenna deployment status')
    while True:
        uv_ant = ant_deploy.Report_deployment_status(uv_addr)
        if uv_ant[0] == 0 and uv_ant[4] == 0 and uv_ant[8] == 0 and uv_ant[12] == 0:
            print('UV ant: DEPLOYED')
            uv_ant = True
        else:
            print('UV ant: NOT DEPLOYED')
        time.sleep(0.5)
        lora_ant = ant_deploy.Report_deployment_status(lora_addr)
        if lora_ant[0] == 0 and lora_ant[4] == 0 and lora_ant[8] == 0 and lora_ant[12] == 0:
            print('LoRa ant: DEPLOYED')
            lora_ant = True
        else:
            print('LoRa ant: NOT DEPLOYED')
        if uv_ant == True and lora_ant == True:
            break
        else:
            print('\nRETRY')
    time.sleep(1)
    print('Deployment verification done')
    time.sleep(1)
    print('Start detumbling')
    time.sleep(3)
else:
    print('Check the antenna deployment status')
    while True:
        uv_ant = ant_deploy.Report_deployment_status(uv_addr)
        if uv_ant[0] == 0 and uv_ant[4] == 0 and uv_ant[8] == 0 and uv_ant[12] == 0:
            print('UV ant: DEPLOYED')
            uv_ant = True
        else:
            print('UV ant: NOT DEPLOYED')
            #TODO: retry with deploy command
        time.sleep(0.5)
        lora_ant = ant_deploy.Report_deployment_status(lora_addr)
        if lora_ant[0] == 0 and lora_ant[4] == 0 and lora_ant[8] == 0 and lora_ant[12] == 0:
            print('LoRa ant: DEPLOYED')
            lora_ant = True
        else:
            print('LoRa ant: NOT DEPLOYED')
            #TODO: retry with deploy command
        if uv_ant == True and lora_ant == True:
            break
        else:
            print('\nRETRY')
    time.sleep(1)
    print('Deployment verification done')
    time.sleep(1)
    print('Initializing done')

while True:
    main()