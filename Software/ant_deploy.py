#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
from datetime import datetime
from smbus2 import SMBus, i2c_msg

'''
Created on Tue Jul 21 12:51 2020

@author: Nuttawat Punpigul

Command fuction of the LoRa deployable antenna system for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1

uv_addr = 0x31  # UV I2C address
lora_addr = 0x33  # LoRa I2C address

def request_data(i2c_addr, CC, size, param = [], show = False):
    data = [CC] + param
    write = i2c_msg.write(i2c_addr, data)
    read = i2c_msg.read(i2c_addr, size)
    bus.i2c_rdwr(write)
    time.sleep(0.5)
    bus.i2c_rdwr(read)
    data = list(read)
    if show:
        print(data)
    return data

def Reset(ant_addr):
    request_data(ant_addr, 0xAA, 0)
    if ant_addr == uv_addr:
        print('Reset UV antenna')
    if ant_addr == lora_addr:
        print('Reset LoRa antenna')

def Report_deployment_status(ant_addr, show = False):
    response = request_data(ant_addr, 0xC3, 2)
    status1 = list(map(int, f'{response[1]:08b}'))
    status2 = list(map(int, f'{response[0]:08b}'))
    if show:
        print('Status Report')
        print('[1S,1T,1B,0,2S,2T,2B,IG] [3S,3T,3B,IB,4S,4T,4B,ARM]')
        print(status1, status2)
    status = status1 + status2
    return status

def Measure_antenna_system_temperature(ant_addr):
    vcc = 3.3
    temp_raw_a = request_data(ant_addr, 0xC0, 2)
    raw_a = (temp_raw_a[0]<<8 | temp_raw_a[1])
    vout_a = (vcc/1023) * raw_a
    temp_raw_b = request_data(ant_addr + 1, 0xC0, 2)
    raw_b = (temp_raw_b[0]<<8 | temp_raw_b[1])
    vout_b = (vcc/1023) * raw_b
    print('\n>>> Temp A', round(vout_a, 2),'C, Temp B', round(vout_b, 2), 'C')
    return vout_a, vout_b

def Arm_antenna_system(ant_addr):
    try:
        request_data(ant_addr, 0xAD, 0)
        time.sleep(0.5)
        arm_check = Report_deployment_status(ant_addr)
        if arm_check[15] == 1:
            if ant_addr == uv_addr:
                print('>>> UV antenna Armed <<<')
            if ant_addr == lora_addr:
                print('>>> LoRa antenna Armed <<<')
        else:
            print('!!! Cannot ARM !!!')
    except:
        print('!!! Cannot ARM !!!')

def Disarm_antenna_system(ant_addr):
    try:
        request_data(ant_addr, 0xAC, 0)
        time.sleep(0.5)
        arm_check = Report_deployment_status(ant_addr)
        if arm_check[15] == 0:
            if ant_addr == uv_addr:
                print('>>> UV antenna Disarmed <<<')
            if ant_addr == lora_addr:
                print('>>> LoRa antenna Disarmed <<<')
        else:
            print('!!! Cannot DISARM !!!')
    except:
        print('!!! Cannot DISARM !!!')

def Deploy_antenna(ant_addr, ant_num, show = False):
    if ant_num == 1:
        CC = 0xA1
        bit_num = 0
    if ant_num == 2:
        CC = 0xA2
        bit_num = 4
    if ant_num == 3:
        CC = 0xA3
        bit_num = 8
    if ant_num == 4:
        CC = 0xA4
        bit_num = 12
    deploy_timeout = [5]
    status = Report_deployment_status(ant_addr)
    time.sleep(1)
    if status[bit_num] == 0:
        print('\nDeployed')
        return True
    else:
        print('\nDeploying activated')
        request_data(ant_addr, CC, 0, deploy_timeout)
        time.sleep(deploy_timeout[0])
        if show:
            print('Deploy timeout', deploy_timeout, 's')
            print('Deploying...')
            print('Timeout ---> Check the ANT')
    status = Report_deployment_status(ant_addr)
    time.sleep(1)
    if status[bit_num] == 0:
        print('\n>>Successful<<')
        return True
    else:
        print('\n>>Failed<<')
        return False 

def Start_deploy(ant_addr):
    for i in range(4):
        print('Ant no.', i+1)
        Deploy_antenna(ant_addr,i+1)
        time.sleep(0.5)

######################################
def main():
    print('\n')
    print('1: Report deployment status')
    print('2: Arm antenna system')
    print('3: Disarm antenna system')
    print('4: Deploy antenna')
    print('5: Measure antenna system temperature')
    print('6: ')
    print('7: ')
    print('8: ')
    print('9: ')
    print('00: Manual request')
    print('\n')

    cmd = input("Select: ")
    print('\n1: UV antenna')
    print('2: LoRa antenna')
    ant_select = int(input('Select antenna:'))
    if ant_select == 1:
        ant_addr = uv_addr
        print('\n< UV selected >')
    if ant_select == 2:
        ant_addr = lora_addr
        print('\n< LoRa selected >')
    if cmd == '1':
        Report_deployment_status(ant_addr, True)
    if cmd == '2':
        Arm_antenna_system(ant_addr)
    if cmd == '3':
        Disarm_antenna_system(ant_addr)
    if cmd == '4':
        ant_num = int(input("Deploy ant number:"))
        Deploy_antenna(ant_addr, ant_num)
    if cmd == '5':
        Measure_antenna_system_temperature(ant_addr)
    if cmd == '00':
        try:
            CC = int((input('Command address(hex):')), 16)
            size = int(input('Response size:'))
            paramin = input('Input param(list, comma delimiter):')
            paramlist = paramin.split(",")
            param = list(map(int, paramlist))
            request_data(CC, size, param)
        except Exception as e:
            print(e)
            print('Invalid input')
            pass

if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print('\nStop config LoRa antenna deployment system')
            break
