#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sys
from datetime import datetime
from smbus2 import SMBus, i2c_msg

'''
Created on Thu Sep 23 13:25 2020

@author: Nuttawat Punpigul

Looping the Reset watchdog fuction of the electronic power subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1
sup_addr = 0x46 # supervisor addres

def request_data(i2c_addr, register, size = 0, data = [], read = True):
    time.sleep(0.1)
    buf = [register] + data
    write = i2c_msg.write(i2c_addr, buf)
    bus.i2c_rdwr(write)
    time.sleep(0.1)
    if read:
        read = i2c_msg.read(i2c_addr, size)
        bus.i2c_rdwr(read)
        time.sleep(0.1)
        return list(read)

def Watchdog_reset():
    register = 0xC0
    print('Reset S-band watchdog')
    request_data(sup_addr, register, read = False)

while True:
	sys.stdout.flush()
	Watchdog_reset()
	time.sleep(58)