from itertools import chain, repeat, islice
import time, socket, random, serial, zlib, struct, binascii, progressbar


# Serial connection setup
# RS485
ser = serial.Serial('/dev/ttyUSB0', 3000000, timeout=1)  # open serial connection
# print(ser.isOpen)     #  check the serial connection
# ser.open()
print('Open serial on', ser.name)    #  display psort name

# General header information
Header = [0x45, 0x53, 0x55, 0x50]  # "ESUP" in ASCII character format
mID = [0x05, 0x00]	 # Module ID

# Function for padding zero
# usage: VAR = list(pad(LIST, SIZE, 0x00))
def pad(iterable, size, padding=None):
    return islice(pad_infinite(iterable, padding), size)
def pad_infinite(iterable, padding=None):
    return chain(iterable, repeat(padding))

# GET parameter function
def GET(Type):
    # set up the ESUP packet
    esup1 = Header + mID + [0x00, 0x00] + [0x00, 0x00] + [0x00, 0x01] + Type
    esup1_crc = list((zlib.crc32(bytes(esup1))).to_bytes(length=4,byteorder='little', signed=False))
    esup2 = Header + mID + [0x02, 0x00] + [0x00, 0x00] + [0x14, 0x01] + [0x00, 0x01] + Type
    esup2_crc = list((zlib.crc32(bytes(esup2))).to_bytes(length=4,byteorder='little', signed=False))
    esup3 = Header + mID + [0x00, 0x00] + [0x05, 0x00] + [0x00, 0x01] + Type
    esup3_crc = list((zlib.crc32(bytes(esup3))).to_bytes(length=4,byteorder='little', signed=False))
    req_command = list(pad(esup1 + esup1_crc, 32, 0x00))
    req_result = list(pad(esup2 + esup2_crc, 32, 0x00))
    end_ack = list(pad(esup3 + esup3_crc, 32, 0x00))
    
    # Sent request command            Receive request command ACK
    # print('\n---------- Start communication ----------')
    ser.write(bytes(req_command))
    time.sleep(0.5)
    read = ser.read(32)
    req_ack = list(read)
    print('Host: Sent request COMMAND')
    print(binascii.hexlify(bytes(req_command)))
    print(binascii.hexlify(read))
    if req_ack[8:10] == [0x5,0x0]:
        # print('Module: ACKNOWLEDGE!')
        print()
    if req_ack[8:10] == [0x6,0x0]:
        print('Module: NOT ACK')
    if req_ack[8:10] == [0x7,0x0]:
        print('Module: BUSY')
    if req_ack[8:10] == [0x8,0x0]:
        print('Module: NO COMMAND FOR EXECUTION')
    if req_ack[8:10] == [0x9,0x0]:
        print('Module: CMD STACK FULL')
    if req_ack[8:10] == [0x10,0x0]:
        print('Module: CMD TEMPORARILY NOT ACCEPTED')
    if req_ack[8:10] != [0x5,0x0] and req_ack[8:10] != [0x6,0x0] and req_ack[8:10] != [0x7,0x0] and req_ack[8:10] != [0x8,0x0] and req_ack[8:10] != [0x9,0x0] and req_ack[8:10] != [0x10,0x0]:
        print('Module: Invalid ESUP')
    time.sleep(0.5)
    # Sent request command RESULT     Receive the RESULT
    ser.write(bytes(req_result))
    time.sleep(0.5)
    read = ser.read(32)
    print('\nHost: Sent request RESULT')
    print(binascii.hexlify(bytes(req_result)))
    print(binascii.hexlify(read))
    result = list(read)
    data_length = result[6:8]
    if data_length[0] == 0 or data_length[0] == 1:
        if data_length[0] == 0:
            print('Module: BUSY') 
        if data_length[0] == 1:
            print('Module: ERROR')
        time.sleep(0.5)
        # Sent end ACK packet
        ser.write(bytes(end_ack))
        print('\nHost: Sent end ACK')
        print(binascii.hexlify(bytes(end_ack)))
        # print('---------- End of communication ----------\n')
        return result
    # print('module:', ''.join(['0x%02x '%b for b in result]))
    # print('module: OK!')
    time.sleep(0.5)
    # Sent end ACK packet
    ser.write(bytes(end_ack))
    print('\nHost: Sent end ACK')
    print(binascii.hexlify(bytes(end_ack)))
    # print('---------- End of communication ----------\n')
    return result

# SET parameter function
def SET(Type, Data_length, Data):
    # set up the ESUP packet
    esup1 = Header + mID + Data_length + [0x00, 0x00] + [0x01, 0x01] + Type + Data 
    esup1_crc = list((zlib.crc32(bytes(esup1))).to_bytes(length=4,byteorder='little', signed=False))
    esup2 = Header + mID + [0x02, 0x00] + [0x00, 0x00] + [0x14, 0x01] + [0x01, 0x01] + Type
    esup2_crc = list((zlib.crc32(bytes(esup2))).to_bytes(length=4,byteorder='little', signed=False))
    esup3 = Header + mID + [0x00, 0x00] + [0x05, 0x00] + [0x01, 0x01] + Type
    esup3_crc = list((zlib.crc32(bytes(esup3))).to_bytes(length=4,byteorder='little', signed=False))
    extra = len(esup1 + esup1_crc) % 16
    if extra > 0:
        final_esup1 = esup1 + esup1_crc + ([0x00] * (16 - extra))
    else:
        final_esup1 = esup1 + esup1_crc
    # req_command = list(pad(esup1 + esup1_crc, 32 , 0x00))
    req_command = final_esup1
    req_result = list(pad(esup2 + esup2_crc, 32, 0x00))
    end_ack = list(pad(esup3 + esup3_crc, 32, 0x00))
    
    # Sent request command            Receive request command ACK
    # print('\n---------- Start communication ----------')
    ser.write(bytes(req_command))
    time.sleep(0.5)
    read = ser.read(32)
    req_ack = list(read)
    print('Host: Sent request COMMAND')
    print(binascii.hexlify(bytes(req_command)))
    print(binascii.hexlify(read))
    if req_ack[8:10] == [0x5,0x0]:
        # print('Module: ACKNOWLEDGE!')
        print()
    if req_ack[8:10] == [0x6,0x0]:
        print('Module: NOT ACK')
    if req_ack[8:10] == [0x7,0x0]:
        print('Module: BUSY')
    if req_ack[8:10] == [0x8,0x0]:
        print('Module: NO COMMAND FOR EXECUTION')
    if req_ack[8:10] == [0x9,0x0]:
        print('Module: CMD STACK FULL')
    if req_ack[8:10] == [0x10,0x0]:
        print('Module: CMD TEMPORARILY NOT ACCEPTED')
    if req_ack[8:10] != [0x5,0x0] and req_ack[8:10] != [0x6,0x0] and req_ack[8:10] != [0x7,0x0] and req_ack[8:10] != [0x8,0x0] and req_ack[8:10] != [0x9,0x0] and req_ack[8:10] != [0x10,0x0]:
        print('Module: Invalid ESUP')
    time.sleep(0.5)
    # Sent request command RESULT     Receive the RESULT
    ser.write(bytes(req_result))
    time.sleep(0.5)
    read = ser.read(32)
    print('\nHost: Sent request RESULT')
    print(binascii.hexlify(bytes(req_result)))
    print(binascii.hexlify(read))
    result = list(read)
    if result[6:10] == [0x00, 0x00, 0x07, 0x00]:
        print('Module: BUSY')
        time.sleep(0.5)
        ser.write(bytes(end_ack))
        print('\nHost: Sent end ACK')
        # print('---------- End of communication ----------\n')
        return
    if result[14] == 0:
        # print('Module: OK!')
        print()
    if result[14] == 1:
        print('Module: System Error')
    if result[14] == 2:
        print('Module: Busy')
    if result[14] == 3:
        print('Module: Wrong parameter')
    time.sleep(0.5)
    # Sent end ACK packet
    ser.write(bytes(end_ack))
    print('\nHost: Sent end ACK')
    print(binascii.hexlify(bytes(end_ack)))
    # print('---------- End of communication ----------\n')
    return result

# TODO: Response of result is not the same value in User manual
# System config function
def CONFIG(command):
    # set up the ESUP packet
    esup1 = Header + mID + [0x00, 0x00] + [0x00, 0x00] + command + [0x00, 0x00]
    esup1_crc = list((zlib.crc32(bytes(esup1))).to_bytes(length=4,byteorder='little', signed=False))
    esup2 = Header + mID + [0x00, 0x00] + [0x00, 0x00] + [0x14, 0x01] + command
    esup2_crc = list((zlib.crc32(bytes(esup2))).to_bytes(length=4,byteorder='little', signed=False))
    esup3 = Header + mID + [0x00, 0x00] + [0x05, 0x00] + command + [0x00, 0x00]
    esup3_crc = list((zlib.crc32(bytes(esup3))).to_bytes(length=4,byteorder='little', signed=False))
    req_command = list(pad(esup1 + esup1_crc, 32, 0x00))
    req_result = list(pad(esup2 + esup2_crc, 32, 0x00))
    end_ack = list(pad(esup3 + esup3_crc, 32, 0x00))
    
    # Sent request command            Receive request command ACK
    # print('\n---------- Start communication ----------')
    ser.write(bytes(req_command))
    time.sleep(0.5)
    read = ser.read(32)
    req_ack = list(read)
    print('Host: Sent request COMMAND')
    print(binascii.hexlify(bytes(req_command)))
    print(binascii.hexlify(read))
    if req_ack[8:10] == [0x5,0x0]:
        # print('Module: ACKNOWLEDGE!')
        print()
    if req_ack[8:10] == [0x6,0x0]:
        print('Module: NOT ACK')
    if req_ack[8:10] == [0x7,0x0]:
        print('Module: BUSY')
    if req_ack[8:10] == [0x8,0x0]:
        print('Module: NO COMMAND FOR EXECUTION')
    if req_ack[8:10] == [0x9,0x0]:
        print('Module: CMD STACK FULL')
    if req_ack[8:10] == [0x10,0x0]:
        print('Module: CMD TEMPORARILY NOT ACCEPTED')
    if req_ack[8:10] != [0x5,0x0] and req_ack[8:10] != [0x6,0x0] and req_ack[8:10] != [0x7,0x0] and req_ack[8:10] != [0x8,0x0] and req_ack[8:10] != [0x9,0x0] and req_ack[8:10] != [0x10,0x0]:
        print('Module: Invalid ESUP')

    print('Wait the module to execute the command for 30 s')
    for i in progressbar.progressbar(range(30)):
        time.sleep(1)

    # Sent request command RESULT     Receive the RESULT
    ser.write(bytes(req_result))
    time.sleep(0.5)
    read = ser.read(32)
    print('\nHost: Sent request RESULT')
    print(binascii.hexlify(bytes(req_result)))
    print(binascii.hexlify(read))
    result = list(read)
    if result[8] == 0x08:
        # print('Module: OK!')
        print()
    else:
        print('Module: System Error - Configuration Not Completed Successfully')
    time.sleep(0.5)
    # Sent end ACK packet
    ser.write(bytes(end_ack))
    print('\nHost: Sent end ACK')
    print(binascii.hexlify(bytes(end_ack)))
    # print('---------- End of communication ----------\n')
    return result

def cmd_result(command, Data = [], Data_length = [0x00, 0x00], frame_size = 0, Type = [0x00, 0x00], delay_time = 0.5, result_length = [0x00, 0x00], Type_ = []):
    esup2 = Header + mID + result_length + [0x00, 0x00] + [0x14, 0x01] + command + Type_
    esup2_crc = list((zlib.crc32(bytes(esup2))).to_bytes(length=4,byteorder='little', signed=False))
    esup3 = Header + mID + [0x00, 0x00] + [0x05, 0x00] + command + Type
    esup3_crc = list((zlib.crc32(bytes(esup3))).to_bytes(length=4,byteorder='little', signed=False))    
    req_result = list(pad(esup2 + esup2_crc, 32, 0x00))
    end_ack = list(pad(esup3 + esup3_crc, 32, 0x00))
    # Sent request command RESULT     Receive the RESULT
    ser.write(bytes(req_result))
    time.sleep(0.5)
    read = ser.readall()
    # read = ser.read(128)
    print('\nHost: Sent request RESULT')
    print(binascii.hexlify(bytes(req_result)))
    print(binascii.hexlify(read))
    result = list(read)
    if result[8:10] == [0x6,0x0]:
        print('Module: NOT ACK')
    if result[8:10] == [0x7,0x0]:
        print('Module: BUSY')
    if result[8:10] == [0x8,0x0]:
        print('Module: NO COMMAND FOR EXECUTION')
    if result[8:10] == [0x9,0x0]:
        print('Module: CMD STACK FULL')
    if result[8:10] == [0x10,0x0]:
        print('Module: CMD TEMPORARILY NOT ACCEPTED')    
    # print('Module:', binascii.hexlify(read))
    time.sleep(0.5)

    # Sent end ACK packet
    ser.write(bytes(end_ack))
    # print('\nHost: Sent end ACK')
    # print('---------- End of communication ----------\n')
    return result

# File system function
def FILE(command, Data = [], Data_length = [0x00, 0x00], frame_size = 0, Type = [0x00, 0x00], delay_time = 0.5, result_length = [0x00, 0x00], Type_ = []):
    # set up the ESUP packet
    esup1 = Header + mID + Data_length + [0x00, 0x00] + command + Type + Data
    esup1_crc = list((zlib.crc32(bytes(esup1))).to_bytes(length=4,byteorder='little', signed=False))
    esup2 = Header + mID + result_length + [0x00, 0x00] + [0x14, 0x01] + command + Type_
    esup2_crc = list((zlib.crc32(bytes(esup2))).to_bytes(length=4,byteorder='little', signed=False))
    esup3 = Header + mID + [0x00, 0x00] + [0x05, 0x00] + command + Type
    esup3_crc = list((zlib.crc32(bytes(esup3))).to_bytes(length=4,byteorder='little', signed=False))
    extra = len(esup1 + esup1_crc) % 16
    if extra > 0:
        final_esup1 = esup1 + esup1_crc + ([0x00] * (16 - extra))
    else:
        final_esup1 = esup1 + esup1_crc
    req_command = final_esup1
    req_result = list(pad(esup2 + esup2_crc, 32, 0x00))
    end_ack = list(pad(esup3 + esup3_crc, 32, 0x00))
    
    # Sent request command            Receive request command ACK
    # print('\n---------- Start communication ----------')
    ser.write(bytes(req_command))
    time.sleep(0.5)
    read = ser.read(32)
    req_ack = list(read)
    print('Host: Sent request COMMAND')
    print(binascii.hexlify(bytes(req_command)))
    print(binascii.hexlify(read))
    # print(req_ack[8:10])
    if req_ack[8:10] == [0x5,0x0]:
        # print('Module: ACKNOWLEDGE!')
        print()
    if req_ack[8:10] == [0x6,0x0]:
        print('Module: NOT ACK')
    if req_ack[8:10] == [0x7,0x0]:
        print('Module: BUSY')
    if req_ack[8:10] == [0x8,0x0]:
        print('Module: NO COMMAND FOR EXECUTION')
    if req_ack[8:10] == [0x9,0x0]:
        print('Module: CMD STACK FULL')
    if req_ack[8:10] == [0x10,0x0]:
        print('Module: CMD TEMPORARILY NOT ACCEPTED')
    if req_ack[8:10] != [0x5,0x0] and req_ack[8:10] != [0x6,0x0] and req_ack[8:10] != [0x7,0x0] and req_ack[8:10] != [0x8,0x0] and req_ack[8:10] != [0x9,0x0] and req_ack[8:10] != [0x10,0x0]:
        print('Module: Invalid ESUP')
    
    # for send file command we have to wait the module to send the file for some time
    if delay_time != 0.5:
        print('Wait the module to send the file for %d s' % delay_time)
        for i in progressbar.progressbar(range(delay_time)):
            time.sleep(1)
    else:
        time.sleep(delay_time)

    # Sent request command RESULT     Receive the RESULT
    ser.write(bytes(req_result))
    time.sleep(0.5)
    read = ser.readall()
    # read = ser.read(128)
    print('\nHost: Sent request RESULT')
    print(binascii.hexlify(bytes(req_result)))
    print(binascii.hexlify(read))
    result = list(read)
    if result[8:10] == [0x6,0x0]:
        print('Module: NOT ACK')
    if result[8:10] == [0x7,0x0]:
        print('Module: BUSY')
    if result[8:10] == [0x8,0x0]:
        print('Module: NO COMMAND FOR EXECUTION')
    if result[8:10] == [0x9,0x0]:
        print('Module: CMD STACK FULL')
    if result[8:10] == [0x10,0x0]:
        print('Module: CMD TEMPORARILY NOT ACCEPTED')    
    # print('Module:', binascii.hexlify(read))
    time.sleep(0.5)

    # Sent end ACK packet
    ser.write(bytes(end_ack))
    print('\nHost: Sent end ACK')
    print(binascii.hexlify(bytes(end_ack)))
    # print('---------- End of communication ----------\n')
    return result

def DIR():      # DIR command function
    command = [0x02, 0x01]
    Data_length = [0x00, 0x00]
    Data =[]
    raw = FILE(command, Data, Data_length)
    frame_size = int.from_bytes(raw[6:8], byteorder='little', signed=False)
    more_flag = 'NO'
    exec_stat = 'Something wrong!'
    file_read = raw[18:frame_size+14]
    file_quan2 = 0
    time.sleep(0.1)
    while raw[15] == 1:
        more_flag = 'YES'
        command = [0x03, 0x01]
        raw = FILE(command)
        frame_size = int.from_bytes(raw[6:8], byteorder='little', signed=False)
        file_read += raw[18:frame_size+14]                # some bug here!!
        file_quan2 += int.from_bytes(raw[16:18], byteorder='little', signed=False)
    if raw[14] == 0:
        exec_stat = 'OK'
    if frame_size == [0x01, 0x00]:
        print('File Card Error!!')
    else:
        print('DIR data')
        print('Command Execute stat:', exec_stat)
        print('Data length:', int.from_bytes(raw[6:8], byteorder='little', signed=False), 'bytes')
        file_quan1 = int.from_bytes(raw[16:18], byteorder='little', signed=False)
        file_quan = file_quan1 + file_quan2
        print(file_quan, 'files in module')
        # print('File name:', bytes(file_read))
        print('File name:')
        start_read = 0
        for i in range(start_read, file_quan):         # find the Null terminater
            file_name = []
            while file_read[start_read] != 0:           # cut the file name after found the Null terminater
                file_name += [file_read[start_read]]
                start_read += 1
            file_size = int.from_bytes(file_read[start_read+1:start_read+4], byteorder='little', signed=False)      # file size is 4 bytes after the Null terminater
            start_read += 5
            print('\t  ', bytes(file_name).decode("ASCII") , '\t\t', file_size, 'bytes')

        print('More Files Avalible:', more_flag)

while True:
    try:
        print('\n')
        print('GET')
        print('1: GET All parameters')
        print('2: GET Modulator Data Interface')
        print('3: GET Status report')
        print('\nSystem Configuration')
        print('4: Idle mode')
        print('5: TX mode')
        print('6: Safe Shutdown')
        print('\nFile System')
        print('7: DIR')
        print('8: Delete File')
        print('9: Delete All Files')
        print('10: Create File')
        print('11: Open File')
        print('12: Send File')
        print('\nSET')
        print('13: SET Modulator Data Interface')
        print('14: TX power')
        print('\n')

        cmd = input("Select: ")
        if cmd == '1':
            Type = [0x48, 0x00]
            raw = GET(Type)
            symbol_rate = raw[15]
            tx_pow = raw[16]
            mod_code = raw[17]
            if raw[18] == 0: 
                roll_off = '0.35'
            if raw[18] == 1: 
                roll_off = '0.25'
            if raw[18] == 2: 
                roll_off = '0.2'
            if raw[19] == 0:
                pilot_signal = 'OFF'
            if raw[19] == 1:
                pilot_signal = 'ON'
            if raw[20] == 0:
                fec_size = 'NORMAL'
            if raw[20] == 1:
                fec_size = 'SHORT'
            preTX_delay = int.from_bytes(raw[21:23], byteorder='little', signed=False)
            freq = struct.unpack('f', bytes(raw[23:27]))
            print('Symbol Rate:', symbol_rate, 'Msp/s')
            print('TX power:', tx_pow, 'dBm')
            print('MOD-Code:', mod_code, '(1-11: QPSK  12-17: 8PSK  18-23: 16APSK)', '(1/2 QPSK default)')
            print('Roll-off:', roll_off)
            print('Pilot signal:', pilot_signal)
            print('FEC frame size:', fec_size)
            print('Pretransmission delay:', preTX_delay, 'ms')
            print('Center frequency:', freq[0], 'MHz')
        if cmd == '2':
            Type = [0x4D, 0x00]
            raw = GET(Type)
            if raw[15] == 0:
                print('Internal Data Interface - Data coming from SD card files')
            else:
                if raw[16] == 0:
                    conn = 'PC104 Connector'
                else:
                    conn = 'LVDS Connector'
                print('LVDS interface on', conn)
        if cmd == '4':
            print('IDLE mode')
            command = [0x11, 0x01]
            CONFIG(command)
            cmd = '3'
        if cmd == '5':
            print('TX mode')
            command = [0x10, 0x01]
            CONFIG(command)
            cmd = '3'
        if cmd == '6':
            print('Safe Shutdown')
            command = [0x13, 0x01]
            CONFIG(command)
            cmd = '3'
        if cmd == '3':
            Type = [0x49, 0x00]
            raw = GET(Type)
            if raw[15] == 1:
                sys_stat = 'Sys After Reset'
            if raw[15] == 2:
                sys_stat = 'Sys Idle Mode'
            if raw[15] == 3:
                sys_stat = 'Sys Transmission Mode'
            if raw[15] == 4:
                sys_stat = 'Sys Going to Shutdown'
            if raw[16] == 0:
                stat_flag = 'Healthy'
            if raw[16] == 32:
                stat_flag = 'SDR Not Initialized'
            if raw[16] == 8:
                stat_flag = 'System Over Temperature Stop'
            if raw[16] == 40:
                stat_flag = 'SDR Not Initialized + System Over Temperature Stop'
            cpu_temp = struct.unpack('f', bytes(raw[19:23]))
            print('System State:', sys_stat)
            print('Status Flags:', stat_flag)
            print('CPU Temp:', cpu_temp[0], '*C')
            print('Firmware version:', int.from_bytes(raw[23:27], byteorder='little', signed=False), '(Default start version: 10200)')
        if cmd == '13':
            print('\nModulation Data Interface Type')
            print('0: Internal Data Interface - Data coming from SD card files')
            print('1: LVDS interface')
            type_ = int(input('Select: '))
            print('LVDS Input/Output Type')
            print('0: PC104 Connector')
            print('1: LVDS Connector')
            conn_ = int(input('Select: '))
            Type = [0x4D, 0x00]
            Data_length = [0x02, 0x00]
            Data =[type_, conn_]
            raw = SET(Type, Data_length, Data)
        if cmd == '14':
            print('\nTx Power')
            print('TX power range 27-33 dBm')
            tx_ = int(input('Select: '))
            Type = [0x41, 0x00]
            Data_length = [0x01, 0x00]
            Data =[tx_]
            raw = SET(Type, Data_length, Data)
        if cmd == '7':
            DIR()
        if cmd == '12':
            DIR()
            file_select = list((input('\nSelect file:')).encode('ASCII'))
            command = [0x0A, 0x01]
            Data = file_select + [0x00]         # input string + Null terminater
            # print(Data)
            Data_length = list(len(Data).to_bytes(length = 2, byteorder = 'little', signed=False))
            # print(Data_length)
            # TODO: fine tune the sending delay time (not data * 10)
            raw = FILE(command, Data, Data_length, Type = [0x50, 0x00], delay_time = len(Data) * 10, result_length = [0x02, 0x00], Type_=[0x50, 0x00])
            if raw[14] == 0:
                send_stat = 'Operation Successful!!'
            if raw[14] == 1:
                send_stat = 'Not All Files Sent'
            if raw[14] == 2:
                send_stat = 'Card error'
            if raw[14] == 3:
                send_stat = 'File Communication Error'
            if raw[14] == 4:
                send_stat = 'File not ready SYS'
            print('Send Result:', send_stat)
            while input('\nRequest result again? (y/n):') == 'y':
                raw = cmd_result(command, Data, Data_length, Type = [0x50, 0x00], delay_time = len(Data) * 10, result_length = [0x02, 0x00], Type_=[0x50, 0x00])
                if raw[14] == 0:
                    send_stat = 'Operation Successful!!'
                if raw[14] == 1:
                    send_stat = 'Not All Files Sent'
                if raw[14] == 2:
                    send_stat = 'Card error'
                if raw[14] == 3:
                    send_stat = 'File Communication Error'
                if raw[14] == 4:
                    send_stat = 'File not ready SYS'
                print('Send Result:', send_stat)

        if cmd == '8':
            DIR()
            file_select = list((input('\nSelect file:')).encode('ASCII'))
            command = [0x04, 0x01]
            Data = file_select + [0x00]         # input string + Null terminater
            Data_length = list(len(Data).to_bytes(length = 2, byteorder = 'little', signed=False))
            raw = FILE(command, Data, Data_length)
            if raw[14] == 0:
                print('Module: File name', bytes(file_select).decode('ASCII'), 'has been deleted')
            if raw[14] == 1:
                print('Module: File Not Found')
            if raw[14] == 2:
                print('Module: File Card Error')
            if raw[14] == 3:
                print('Module: Wrong file name')
        if cmd == '10':
            DIR()
            print('\nCreate file')
            file_select = list((input('Input file name:')).encode('ASCII'))
            print('\n!!!!! Warning !!!!!\nThis is for testing\nWrite dummy data to the new file(1-16)')
            write_data = list((input('Input data to write:')).encode('ASCII'))
            # write_data = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]           # TODO: dummy data
            command = [0x06, 0x01]          # create file command
            Data = file_select + [0x00] + list((len(write_data)).to_bytes(length = 4, byteorder = 'little', signed=False))
            Data_length = list((len(Data)).to_bytes(length = 2, byteorder = 'little', signed=False))
            raw = FILE(command, Data, Data_length)
            if raw[14] == 1:
                print('Module: File Cannot Be Opened (Internal reasons or file name error)')
            if raw[14] == 0:
                handle_num = raw[15:19]             # TODO: error with wrong handle_num
                print('Module: File created\n        Handle number', raw[14:18], '\nModule: Writing to file')
            command = [0x07, 0x01]          # write file command
            Data = list((len(write_data)).to_bytes(length = 2, byteorder = 'little', signed=False)) + handle_num + [0x00, 0x00, 0x00, 0x00] + write_data
            Data_length = list((len(Data)).to_bytes(length = 2, byteorder = 'little', signed=False))
            raw = FILE(command, Data, Data_length)
            if raw[14] == 0:
                print('Module: Create file successful!!')
            if raw[14] == 1:
                print('Module: File Invalid Handle (Handle value different from Create_file command or File Handle closed in module system)')
            if raw[14] == 2:
                print('Module: Not Enough Space')
            if raw[14] == 3:
                print('Module: Card Error')
            if raw[14] == 4:
                print('Module: File communication error (Different values for length in protocol header and data field)')
            if raw[14] == 5:
                print('Module: Invalid Packet Number')

        input('\n----- Press Any Key -----\n')
    except Exception as e :
        print('Stop config S-band transmitter')
        print(e)
        time.sleep(1)
        break 