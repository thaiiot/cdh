#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv

'''
Created on Sat Sep 14 19:48:42 2019

@author: Nuttawat Punpigul
'''

# get sensors data by read the csv file
def send_sensor(select_row):
  with open('payload.csv', 'r+') as readfile:
    reader = csv.DictReader(readfile)
    for row in reader:
      if int(row['Device']) == select_row:
        temp = row['Temp']
        humid = row['Humid']
        select_row += 1
        return temp, humid, select_row