#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sys
from datetime import datetime
from smbus2 import SMBus, i2c_msg

'''
Created on Thu Sep 23 13:25 2020

@author: Nuttawat Punpigul

Looping the Reset watchdog fuction of the electronic power subsystem for the THAIIOT 3U cubesat. Using I2C as the main bus.

TODO: 	- make this script running on system bootup
'''

bus = SMBus(1) # indicates /dev/ic2-1

i2c_addr = 0x20  # EPS I2C address
STID = 0x1A  # System Type Identifier (ICEPS)
IVID = 0x05  # Interface Version Identifier
BID = 0x01	 # Board Identifier

def hex_show(data):
	h = []
	for buf in data:
		h.append(hex(buf))
	print(h)
	# return h

def request_data(CC, size, param = []):
	data = [STID, IVID, CC, BID] + param
	time.sleep(0.1)
	write = i2c_msg.write(i2c_addr, data)
	read = i2c_msg.read(i2c_addr, size)
	bus.i2c_rdwr(write, read)
	data = list(read)
	if data[2] != (CC + 1):
		# print('Wrong Response, retrying...')
		time.sleep(0.1)
		bus.i2c_rdwr(write, read)
		data = list(read)
	hex_show(data)
	print('')
	if data[4] == 0x80:
		print("Response: Accepted")
	if data[4] == 0x81:
		print("Response: Rejected")
	if data[4] == 0x82:
		print("Response: Invalid command code")
	if data[4] == 0x83:
		print("Response: Parameter missing")
	if data[4] == 0x84:
		print("Response: Parameter invalid")
	if data[4] == 0x85:
		print("Response: Unavailable in current mode/configuration")
	if data[4] == 0x86:
		print("Response: invalid system type, interface version or bid")
	if data[4] == 0x87:
		print("Response: internal error occurred during processing")
	return data

def Watchdog_reset():
	print("\n(0x06) Resets the watchdog timer")
	request_data(0x06, 5)

while True:
	sys.stdout.flush()
	Watchdog_reset()
	time.sleep(46)