# 1 Subsystem Introduction

Comparing the satellite system to the human body, CDH is the brain. Mission procedure, system analysis, data sorting, decision making including troubleshoot some basic problems in the satellite will be responsible by this subsystem.

This document will explain all the detail of THAIIOT&#39;s CDH

Initially, document describes the CONOPS of the THAIIOT satellite, as the CDH design must be based on the procedure of the CONOPS.

From CONOPS, it is possible to determine the capabilities that CDH must have before it can be selected for the right hardware to act as the CDH of the satellite. Due to the environment in which the satellite will operate is different from the normal terrestrial climate, studying other satellites that successfully using the same type of hardware that we have chosen is a very necessary step to reduce the risk of using inappropriate hardware to operate in space.

Next, a detailed description of the CDH design, both hardware and software, includes a detailed description of the Flight software, which is CDH&#39;s most important software.

As for the details of the ICD and the command that CDH uses to operate the subsystem that the THAIIOT research team did not develop by themselves will reference the command details from the subsystem&#39;s documentation instead.

Finally, the results of various CDH performance tests and environment tests will be discussed.

![](Figure_1_THAIIOT_s_CONOPS.png)

**Figure 1: THAIIOT&#39;s CONOPS**

# 2 Subsystem Requirement

## 2.1 Concept of Operations

Concept of Operations (CONOPS) is the sequence and process in the operation of the THAIIOT satellite throughout the mission. CDH&#39;s main function is to control and verify that every subsystem performs its own mission in accordance with CONOPS. The CONOPS is divided into 2 phases as follows

### 2.1.1 Deploying phase

![](Deploying_phase_of_CONOPS.png)

**Figure 2: Deploying phase of CONOPS**

This phase is the initial phase after the satellite has been launched from the rocket. The satellite will be in idle state for 30 minutes (to prevent satellite errors during launch and affect other satellites in the same rocket). After this time, it will begin the detumbling process to reduce the satellite&#39;s angular velocity to as close as possible to 0 deg/s. Next will be a very important step of this phase, which is to deploy all the satellite antennas. The satellite will ignore the antennas deploying procedure if it fails 3 times due to the relatively high energy consumption of the antenna deploying. Once the antenna deploying process is complete, the satellite will begin emitting a beacon signal every 1 minute to broadcast the satellite&#39;s status to the GCS then begin the next phase.

### 2.1.2 Orbiting phase

![](Orbiting_phase_of_CONOPS.png)

**Figure 3: Orbiting phase of CONOPS**

The satellite starts its normal mission cycle in this phase. The beacon broadcasting every 1 minute is what the satellite will do in standby mode. Receiving the satellite&#39;s beacon will allow GCS to recognize that a satellite is approaching into the GCS&#39;s airspace, where it can begin communicating with the satellite. According to mission analysis, GCS get 15 minutes to communicate with the satellite before it gets out of the communication range. In the first 5 minutes, housekeeping data is collected by the GCS (a telemetry contained the detail of each subsystem status) to assess the current state of the satellite if each satellite&#39;s sub system is damaged or not. Dumping data collected by satellites (LoRa&#39;s data) to the GCS and launching mission mode is determined by the GCS, based on housekeeping data received from satellites. Another 10 minutes will be used for data dumping and mission mode. Regardless of whether it has entered mission mode or not, once the satellite is out of range to communicate with the GCS, the satellite will return to standby mode (broadcast the beacon every 1 minute), ending one cycle.

The data obtained from data dumping is obtained from mission mode enabled, so the data received by GCS will be data from the last mission mode activated, saved on a separate file per day, not data collected by the satellite at the time data dumping was requested. GCS can specify which file to dump from the satellite.

## 2.2 Function of the CDH

As the chief of all subsystems, CDH requires high compatibility and survivability than other subsystem, so we can conclude the function of CDH as follow

- Support I2C and UART for communicate with other subsystems
- Support SPI for communicate with a LoRa chip
- Able to host a standalone LoRa server
- Large capacity, High read-write sustainable and long-lasting memory
- More than 1 Watchdog
- Consume as low power as possible

- To comply with the amount of data that the satellite may collect over its lifetime. At least 2 gigabytes (GB) of the storage device should be installed
- Have a power management system, to protect the subsystem from abnormal powering

## 2.3 Analysis of the CDH subsystem characteristics

From the requirements mentioned above. The key point in choosing the right hardware for the CDH is that it can host a custom LoRa server. Chirpstack Lora Server[1] is the only open source LoRa server at this time, which is designed to run on the Linux operating system. This narrowed the scope of CDH hardware selection to a group of SBC that capable of running Linux operating system and Chirpstack LoRa Server. We have selected commercially available SBC to test the OS and LoRa server installations. The results are shown in Table 1, All parameters related to requirement in Section 2.2 are considered.

|
 | Raspberry pi zero | Raspberry pi 3B | Raspberry pi 4 | Orange pi Zero | Radxa zero | Beaglebone black |
| --- | --- | --- | --- | --- | --- | --- |
| Processor |
 |
 |
 |
 |
 |
 |
| RAM |
 |
 |
 |
 |
 |
 |
| Storage |
 |
 |
 |
 |
 |
 |
| OS support |
 |
 |
 |
 |
 |
 |
| LoRa ServerCompatibility |
 |
 |
 |
 |
 |
 |
| Power consumption |
 |
 |
 |
 |
 |
 |
| I2C |
 |
 |
 |
 |
 |
 |
| UART |
 |
 |
 |
 |
 |
 |
| SPI |
 |
 |
 |
 |
 |
 |
| Temp. range |
 |
 |
 |
 |
 |
 |

**Table 1: SBC comparisons**

Conclusion of Table 1

Pros and Cons of RPI zero

Literature review, raspberry pi space proof

![](RaspberryPiSpaceProof1.jpg) ![](RaspberryPiSpaceProof2.jpg)

DoT-1 SSTL

# 3 Subsystem Overview and Specification

## 3.1 THAIIOT subsystem architecture

THAIIOT satellite system has been designed in modular design. A distributed architecture allows easier development of each subsystem and simplify test procedures as a lot of tests can be done before integration of subsystems, so each subsystem team can develop the subsystem independently.

![](CDH_Subsystem_diagram.png)

**Figure 4: Subsystem diagram**

THAIIOT satellites consist of 7 subsystems, including both non-researched and self-developed subsystems. In Figure 4, the blue box represents the non-researched subsystem and the green is the research-developed subsystem. myself Due to the creation of satellites, cubesat is popular today. Therefore, there are many private companies that have researched and developed a basic bus system with high quality satellites for us to choose from. The TAIOT research team therefore focuses on developing the payload of the satellite, which is the part that each satellite has different. rather than the development of a basic bus system where all satellites are already similar

## 3.2 Data system diagram

![](CDHData_system_diagram.png)
